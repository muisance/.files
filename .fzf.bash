# Setup fzf
# ---------
if [[ ! "$PATH" == */home/m3w/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/m3w/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/m3w/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/m3w/.fzf/shell/key-bindings.bash"
