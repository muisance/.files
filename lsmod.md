Module                  Size  Used by
ccm                    20480  9
lzo_rle                16384  8
zram                   36864  2
snd_hda_codec_hdmi     73728  1
snd_hda_codec_realtek   143360  1
snd_hda_codec_generic    98304  1 snd_hda_codec_realtek
ledtrig_audio          16384  1 snd_hda_codec_generic
iwldvm                278528  0
mac80211             1052672  1 iwldvm
i915                 2662400  2
libarc4                16384  1 mac80211
iwlwifi               401408  1 iwldvm
intel_rapl_msr         20480  0
intel_rapl_common      32768  1 intel_rapl_msr
x86_pkg_temp_thermal    20480  0
btusb                  69632  0
intel_powerclamp       20480  0
btrtl                  24576  1 btusb
coretemp               20480  0
btbcm                  20480  1 btusb
btintel                32768  1 btusb
mei_hdcp               24576  0
bluetooth             741376  5 btrtl,btintel,btbcm,btusb
i2c_algo_bit           16384  1 i915
kvm_intel             323584  0
snd_hda_intel          57344  0
drm_kms_helper        266240  1 i915
snd_intel_dspcfg       24576  1 snd_hda_intel
snd_hda_codec         167936  4 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec_realtek
cfg80211              913408  3 iwldvm,iwlwifi,mac80211
kvm                   851968  1 kvm_intel
snd_hda_core          106496  5 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek
iTCO_wdt               16384  0
intel_pmc_bxt          16384  1 iTCO_wdt
cec                    73728  2 drm_kms_helper,i915
alx                    57344  0
ecdh_generic           16384  1 bluetooth
ecc                    36864  1 ecdh_generic
snd_hwdep              16384  1 snd_hda_codec
iTCO_vendor_support    16384  1 iTCO_wdt
msi_wmi                20480  0
mxm_wmi                16384  0
mdio                   16384  1 alx
at24                   24576  0
rc_core                61440  1 cec
snd_pcm               147456  4 snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec,snd_hda_core
sparse_keymap          16384  1 msi_wmi
intel_gtt              24576  1 i915
mousedev               24576  0
input_leds             16384  0
joydev                 28672  0
snd_timer              45056  1 snd_pcm
snd                   114688  8 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hwdep,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek,snd_timer,snd_pcm
syscopyarea            16384  1 drm_kms_helper
sysfillrect            16384  1 drm_kms_helper
sysimgblt              16384  1 drm_kms_helper
rfkill                 28672  4 bluetooth,cfg80211
irqbypass              16384  1 kvm
fb_sys_fops            16384  1 drm_kms_helper
soundcore              16384  1 snd
crct10dif_pclmul       16384  1
mei_me                 49152  1
mei                   126976  3 mei_hdcp,mei_me
crc32_pclmul           16384  0
ac                     16384  0
evdev                  28672  9
mac_hid                16384  0
battery                20480  0
i2c_i801               36864  0
ghash_clmulni_intel    16384  0
aesni_intel           372736  6
lpc_ich                28672  0
i2c_smbus              20480  1 i2c_i801
crypto_simd            16384  1 aesni_intel
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
psmouse               184320  0
pcspkr                 16384  0
glue_helper            16384  1 aesni_intel
wmi                    36864  2 msi_wmi,mxm_wmi
rapl                   16384  0
intel_cstate           20480  0
intel_uncore          172032  0
drm                   585728  3 drm_kms_helper,i915
fuse                  139264  1
agpgart                53248  2 intel_gtt,drm
pkcs8_key_parser       16384  0
ip_tables              32768  0
x_tables               53248  1 ip_tables
ext4                  815104  1
crc32c_generic         16384  0
crc16                  16384  2 bluetooth,ext4
mbcache                16384  1 ext4
jbd2                  139264  1 ext4
hid_generic            16384  0
usbhid                 65536  0
hid                   147456  2 usbhid,hid_generic
serio_raw              20480  0
rtsx_pci_sdmmc         32768  0
atkbd                  36864  0
libps2                 20480  2 atkbd,psmouse
crc32c_intel           24576  2
sdhci_pci              61440  0
cqhci                  32768  1 sdhci_pci
sr_mod                 28672  0
sdhci                  77824  1 sdhci_pci
cdrom                  77824  1 sr_mod
mmc_core              188416  4 rtsx_pci_sdmmc,sdhci,cqhci,sdhci_pci
xhci_pci               20480  0
xhci_pci_renesas       20480  1 xhci_pci
ehci_pci               20480  0
rtsx_pci              106496  1 rtsx_pci_sdmmc
xhci_hcd              286720  1 xhci_pci
ehci_hcd               98304  1 ehci_pci
i8042                  32768  0
serio                  28672  6 serio_raw,atkbd,psmouse,i8042
