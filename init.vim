set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
let mapleader=","

call plug#begin('~/.config/nvim/plugged')
Plug 'xabikos/vscode-javascript'
Plug 'neoclide/coc.nvim'
Plug 'junegunn/goyo.vim'
Plug 'cespare/vim-toml'
Plug 'neoclide/coc-eslint'
Plug 'leafgarland/typescript-vim'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'neoclide/coc-tsserver'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-surround'
Plug 'grvcoelho/vim-javascript-snippets'
Plug 'w0rp/ale'
Plug 'isruslan/vim-es6'
Plug 'ryuta69/elly.vim'
Plug 'dracula/vim'
Plug 'franbach/miramare'
Plug 'liuchengxu/space-vim-dark'
Plug 'glepnir/oceanic-material'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'nanotech/jellybeans.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'christianchiarulli/nvcode-color-schemes.vim'
Plug 'kaicataldo/material.vim', { 'branch': 'main' }
Plug 'tomasiser/vim-code-dark'
Plug 'flazz/vim-colorschemes'
Plug 'chun-yang/auto-pairs'
Plug 'eagletmt/neco-ghc'
Plug 'junegunn/vim-easy-align'
Plug 'https://github.com/junegunn/vim-github-dashboard'
Plug 'shougo/neocomplete.vim'
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-markdown'
Plug 'ap/vim-css-color'
Plug 'ryanoasis/vim-devicons'
call plug#end()

set background=dark
set wrap
set acd
set is
set ic
set scs
set nu
set rnu
set hls
set cul
set spr
set title
set mouse=a
set slm=mouse
set sta
set et
set bin
set icon
set expandtab
set clipboard+=unnamedplus
set tgc
set sft
set sw=4
set sts=4



" colorscheme space-vim-dark
" colorscheme material
" colorscheme nvcode
" colorscheme wasabi256
" colorscheme sonokai
" colorscheme deus
colorscheme ayu
" colorscheme gruvbox
" colorscheme PaperColor


" For Neovim 0.1.3 and 0.1.4 - https://github.com/neovim/neovim/pull/2198
if (has('nvim'))
  let $NVIM_TUI_ENABLE_TRUE_COLOR = 1
endif
" For Neovim > 0.1.5 and Vim > patch 7.4.1799 - https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162
" Based on Vim patch 7.4.1770 (`guicolors` option) - https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd
" https://github.com/neovim/neovim/wiki/Following-HEAD#20160511
if (has('termguicolors'))
  set termguicolors
endif

lua require'colorizer'.setup()

let g:goyo_width=120
" let g:material_theme_style = 'ocean-community'
"| 'palenight' | 'lighter' | 'ocean' | 'default-community' | 'palenight-community' | 'ocean-community' | 'lighter-community' | 'darker-community'
" let g:material_terminal_italics = 1
let g:webdevicons_enable = 1
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_enable_airline_tabline = 1
let g:webdevicons_enable_airline_statusline = 1
let g:webdevicons_enable_ctrlp = 1
let g:neocomplete#enable_at_startup = 1
let g:typescript_compiler_binary='tsc'
let g:typescript_compiler_options='--lib es6'
let g:neocomplete#enable_at_startup=1
let g:airline#extensions#tabline#enabled=1
" let g:deepspace_italics=1
let g:airline_theme='ayu'
let g:user_emmet_mode='inv'
let g:user_emmet_leader_key='<C-Z>'
let g:oceanic_material_transparent_background=1
let g:oceanic_material_allow_bold=1
let g:oceanic_material_allow_italic=1

" highlight Cursor guifg=steelblue guibg=orange
" highlight iCursor guifg=steelblue guibg=red
highlight iCursor gui=NONE guifg=bg guibg=red
set guicursor=n-v-c:block,i-ci-ve:ver10,r-cr:hor20,o:hor50
\,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
\,sm:block-blinkwait175-blinkoff150-blinkon175



"Notes for multi-cursor plugin mappings and such:
"!!!IMPORTANT!!!  
"If you spawn multiple cursors in a Normal Mode and enter Insert Mode,
"the cursors will disappear, but will still be in place

"Basic usage:
"select words with Ctrl-N (like Ctrl-d in Sublime Text/VS Code)
"create cursors vertically with Ctrl-Down/Ctrl-Up
"select one character at a time with Shift-Arrows
"press n/N to get next/previous occurrence
"press [/] to select next/previous cursor
"press q to skip current and get next occurrence
"press Q to remove current cursor/selection
"start insert mode with i,a,I,A
"Two main modes:
"in cursor mode commands work as they would in normal mode
"in extend mode commands work as they would in visual mode
"press Tab to switch between «cursor» and «extend» mode

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" coc config
let g:coc_global_extensions = [
	\ 'coc-snippets',
	\ 'coc-pairs',
	\ 'coc-eslint',
	\ 'coc-prettier',
	\ 'coc-json',
	\ ]

" autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }

inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
