
# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
PATH="$HOME/.local/bin${PATH:+:${PATH}}"
#antibody bundle denysdovhan/spaceship-prompt
export HOME=/home/m3w

# Path to your oh-my-zsh installation.
export ZSH="/home/m3w/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="random"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
ZSH_THEME_RANDOM_CANDIDATES=(
                             "dallas"
                             "dieter"
                             "3den"
                             "arrow"
                             "dst"
                             "agnoster"
                             "frisk"
                             "josh"
                             "tjkirch"
                             "philips"
                             "fletcherm"
                             "flazz"
                             "pygmalion"
                             "maran"
                             "jtriley"
                             "wedisagree"
                             "wuffers"
                             "muse"
                             "intheloop"
                             "theunraveler"
                             "sorin"
                             "gallois"
                             "eastwood"
                             "crcandy"
                             "obraun"
                             "superjarin"
                             "jaischeema"
                             "jnrowe"
                             "geoffgarside"
                             "cypher"
                             "re5et"
                             "pygmalion-virtualenv"
                             "nicoulaj"
                             "takashiyoshida"
                             "maran"
                             "intheloop"
                             "tonotdo"
                             "wuffers"
                             "superjarin"
                             "crunch"
                             "nanotech"
                             "candy"
                             "sonicradish"
                             "dpoggi"
                             "ys"
                             "intheloop"
                             "muse"
                             )

# SPACESHIP_PROMPT_ORDER=(
#                         time          # Time stamps section
#                         user          # Username section
#                         dir           # Current directory section
#                         host          # Hostname section
#                         git           # Git section (git_branch + git_status)
#                         package       # Package version
#                         node          # Node.js section
#                         ruby          # Ruby section
#                         haskell       # Haskell Stack section
#                         docker        # Docker section
#                         venv          # virtualenv section
#                         exec_time     # Execution time
#                         line_sep      # Line break
#                         battery       # Battery level and status
#                         vi_mode       # Vi-mode indicator
#                         jobs          # Background jobs indicator
#                         exit_code     # Exit code section
#                         char          # Prompt character
#                         )


#Prompt
#| Variable                             | Default | Meaning                                          |
#| :----------------------------------- | :-----: | ------------------------------------------------ |
#| `SPACESHIP_PROMPT_ADD_NEWLINE`       | `true`  | Adds a newline character before each prompt line |
#| `SPACESHIP_PROMPT_SEPARATE_LINE`     | `true`  | Make the prompt span across two lines            |
#| `SPACESHIP_PROMPT_FIRST_PREFIX_SHOW` | `false` | Shows a prefix of the first section in prompt    |
#| `SPACESHIP_PROMPT_PREFIXES_SHOW`     | `true`  | Show prefixes before prompt sections or not      |
#| `SPACESHIP_PROMPT_SUFFIXES_SHOW`     | `true`  | Show suffixes before prompt sections or not      |
#| `SPACESHIP_PROMPT_DEFAULT_PREFIX`    | `via·`  | Default prefix for prompt sections               |
#| `SPACESHIP_PROMPT_DEFAULT_SUFFIX`    | ` `     | Default suffix for prompt section                |

#Char
#| Variable                          | Default                  | Meaning                                                              |
#| :-------------------------------- | :----------------------: | -------------------------------------------------------------------- |
#| `SPACESHIP_CHAR_PREFIX`           |            ` `           | Prefix before prompt character                                       |
#| `SPACESHIP_CHAR_SUFFIX`           |            ` `           | Suffix after prompt character                                        |
#| `SPACESHIP_CHAR_SYMBOL`           |            `➜ `          | Prompt character to be shown before any command                      |
#| `SPACESHIP_CHAR_SYMBOL_ROOT`      | `$SPACESHIP_CHAR_SYMBOL` | Prompt character to be shown before any command for the root user    |
#| `SPACESHIP_CHAR_SYMBOL_SECONDARY` | `$SPACESHIP_CHAR_SYMBOL` | Secondary prompt character to be shown for incomplete commands       |
#| `SPACESHIP_CHAR_COLOR_SUCCESS`    |         `green`          | Color of prompt character if last command completes successfully     |
#| `SPACESHIP_CHAR_COLOR_FAILURE`    |          `red`           | Color of prompt character if last command returns non-zero exit-code |
#| `SPACESHIP_CHAR_COLOR_SECONDARY`  |         `yellow`         | Color of secondary prompt character                                  |

#Time


# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
         git
         alias-finder
         archlinux
         battery
	     colored-man-pages
         colorize
         common-aliases
         copybuffer
         copydir
         copyfile
	     docker
	     emoji
	     emoji-clock
	     emotty
         git-prompt
	     lol
         jsontools
         shrink-path
	     systemd
	     thefuck
         themes
         vi-mode
         web-search
         zsh-interactive-cd
         zsh-navigation-tools
        )

#Antibody
source <(antibody init)
antibody bundle < ~/.zsh_plugins.txt

#Oh My Zsh
source $ZSH/oh-my-zsh.sh

#User configuration

export MANPATH="/usr/local/man:$MANPATH"

#You may need to manually set your language environment
export LANG=en_US.UTF-8

#Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='nvim'
else
  export EDITOR='vim'
fi

#Compilation flags
export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Example aliases
alias gaa='git add --all'
alias gcs='git commit -v --status'
alias gpp='git push -v --progress'
alias btp='/usr/bin/bashtop'
alias neo='/usr/bin/neofetch'
alias V='/bin/code'
alias al='ls -la'
alias rng='ranger'
alias srn='sudo ranger'
alias ..='cd ..'
alias p='pwd'
alias z='/bin/zsh'
alias c='clear'
alias vv='/bin/vim'
alias nv='/bin/nvim'
alias pf='pacman -F --color always '
alias pfx='pacman -Fx --color always '
alias pq='pacman -Q '
alias pu='pacman -U '
alias sps='sudo pacman -S '
alias pfy='sudo pacman -Fy'
alias psy='sudo pacman -Syu'
alias yas='sudo yay -S '
alias yaf='yay -F --color always '
alias yfx='yay -Fx -- color always '
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Open a file with the appropriate application
function open {
    while [ "$1" ] ; do
        xdg-open $1 &> /dev/null
        shift # shift décale les param
    done
}

#STARSHIP
#eval "$(starship init zsh)"
#fpath=($fpath "/home/m3w/.zfunctions")
# Set Spaceship ZSH as a prompt
# autoload -U promptinit; promptinit
# prompt spaceship
