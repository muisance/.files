-- -- --Base-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
import           XMonad                                                                                     -- Base
import           System.IO (hPutStrLn)                                                                      -- Base
import           System.Exit (exitSuccess)                                                                  -- Base
import qualified XMonad.StackSet as W                                                                       -- Base

-- -- --Actions-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
import           XMonad.Actions.CopyWindow (kill1, copyToAll, killAllOtherCopies, runOrCopy)                -- Actions
import           XMonad.Actions.CycleWS                                                                     -- Actions 
import           XMonad.Actions.GridSelect                                                                  -- Actions 
import           XMonad.Actions.MouseResize                                                                 -- Actions 
import           XMonad.Actions.Promote                                                                     -- Actions 
import           XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)                                       -- Actions 
import           XMonad.Actions.WindowGo (runOrRaise)                                                       -- Actions 
import           XMonad.Actions.WithAll (sinkAll, killAll)                                                  -- Actions 
import qualified XMonad.Actions.Search as S                                                                 -- Actions 

-- -- --Data-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
import           Data.Char (isSpace, toUpper)                                                               -- Data
import           Data.Monoid                                                                                -- Data
import           Data.Maybe (isJust)                                                                        -- Data
import qualified Data.Map as M                                                                              -- Data

-- -- --Hooks-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
import           XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))   -- Hooks
import           XMonad.Hooks.EwmhDesktops                                                                  -- Hooks
import           XMonad.Hooks.FadeInactive                                                                  -- Hooks
import           XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))      -- Hooks
import           XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat)                                     -- Hooks
import           XMonad.Hooks.ServerMode                                                                    -- Hooks
import           XMonad.Hooks.SetWMName                                                                     -- Hooks
import           XMonad.Hooks.WorkspaceHistory                                                              -- Hooks

-- -- --Layouts-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
import           XMonad.Layout.GridVariants (Grid(Grid))                                                    -- Layouts
import           XMonad.Layout.Hidden
import           XMonad.Layout.SimplestFloat                                                                   -- Layouts
import           XMonad.Layout.Spiral                                                                       -- Layouts
import           XMonad.Layout.ResizableTile                                                                -- Layouts
import           XMonad.Layout.Tabbed                                                                       -- Layouts
import           XMonad.Layout.ThreeColumns                                                                 -- Layouts

-- -- --Layout modifiers-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
import           XMonad.Layout.LayoutModifier                                                               -- Layouts modifiers
import           XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)                    -- Layouts modifiers
import           XMonad.Layout.Magnifier                                                                    -- Layouts modifiers
import           XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))                               -- Layouts modifiers
import           XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))           -- Layouts modifiers
import           XMonad.Layout.NoBorders                                                                    -- Layouts modifiers
import           XMonad.Layout.Renamed                                                                      -- Layouts modifiers
import           XMonad.Layout.ShowWName                                                                    -- Layouts modifiers
import           XMonad.Layout.Simplest                                                                     -- Layouts modifiers
import           XMonad.Layout.Spacing                                                                      -- Layouts modifiers
import           XMonad.Layout.SubLayouts                                                                   -- Layouts modifiers
import           XMonad.Layout.WindowNavigation                                                             -- Layouts modifiers
import           XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))                        -- Layouts modifiers
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))                     -- Layouts modifiers
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))                                               -- Layouts modifiers

-- -- --Prompt-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
import           XMonad.Prompt                                                                              -- Prompt
import           XMonad.Prompt.Input                                                                        -- Prompt
import           XMonad.Prompt.FuzzyMatch                                                                   -- Prompt
import           XMonad.Prompt.Man                                                                          -- Prompt 
import           XMonad.Prompt.Pass                                                                         -- Prompt 
import           XMonad.Prompt.Shell                                                                        -- Prompt 
import           XMonad.Prompt.Ssh                                                                          -- Prompt 
import           XMonad.Prompt.XMonad                                                                       -- Prompt 
import           Control.Arrow (first)                                                                      -- Prompt 

-- -- --Utilities-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
import           XMonad.Util.EZConfig (additionalKeysP)                                                     -- Utilities
import           XMonad.Util.NamedScratchpad                                                                -- Utilities
import           XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)                                -- Utilities
import           XMonad.Util.SpawnOnce                                                                      -- Utilities

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -

myFont :: String
myFont = "xft:JetBrains Mono NL:bold:size=6:antialias=true:hinting=true"

myModMask :: KeyMask
myModMask = mod4Mask       -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "kitty"   -- Sets default terminal

myBrowser :: String
myBrowser = "brave"               -- Sets qutebrowser as browser for tree select

myEditor :: String
myEditor = myTerminal ++ " -e nvim "    -- Sets vim as editor for tree select

myBorderWidth :: Dimension
myBorderWidth = 4          -- Sets border width for windows

myNormalBorderColor :: String
myNormalBorderColor = "#ff0040"  -- Border color of normal windows

myFocusedBorderColor :: String
myFocusedBorderColor = "#fbfd00"  -- Border color of focused windows

altMask :: KeyMask
altMask = mod1Mask         -- Setting this for use in xprompts

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myStartupHook  :: X ()
myStartupHook  = do
          spawnOnce "nitrogen --restore &"
          spawnOnce "picom &"
          spawnOnce "nm-applet &"
          spawnOnce "xfce4-power-manager &"
          spawnOnce "gpaste-client daemon &"
--        spawnOnce "volumeicon &"
          spawnOnce "trayer --edge top --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --alpha 10 --tint 0x292d3e --height 24 &"
          setWMName "LG3D"

myColorizer :: Window -> Bool -> X (String, String)
myColorizer = colorRangeFromClassName
              (0x28,0x2c,0x34) -- lowest inactive bg
              (0x28,0x2c,0x34) -- highest inactive bg
              (0xc7,0x92,0xea) -- active bg
              (0xc0,0xa7,0x9a) -- inactive fg
              (0x28,0x2c,0x34) -- active fg

-- gridSelect menu layout
mygridConfig :: p -> GSConfig Window
mygridConfig colorizer = (buildDefaultGSConfig myColorizer)
    { gs_cellheight    = 40
    , gs_cellwidth     = 200
    , gs_cellpadding   = 6
    , gs_originFractX  = 0.5
    , gs_originFractY  = 0.5
    , gs_font          = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                        { gs_cellheight   = 40
                        , gs_cellwidth    = 200
                        , gs_cellpadding  = 6
                        , gs_originFractX = 0.5
                        , gs_originFractY = 0.5
                        , gs_font         = myFont
                        }

myAppGrid  = [
              ("Code", "code")
            , ("SublimeText", "sublime_text")
            , ("Atom", "atom")
            , ("Inkscape", "org.inkscape.Inkscape")
            , ("Gimp", "gimp")
            , ("OBS", "obs")
            , ("Vlc", "vlc")
            , ("mpv", "gl")
            , ("Transmission-gtk", "transmission-gtk")
            , ("PCManFM", "pcmanfm")
            , ("Tor Browser", "torbrowser-launcher")
            , ("Brave-browser", "brave-browser")
            , ("Firefox", "firefox")
            , ("Qutebrowser", "qutebrowser")
            , ("Signal", "signal-desktop")
            , ("TelegramDesktop", "telegram-desktop")
            , ("Deadbeef", "deadbeef")
            , ("Nitrogen", "nitrogen")
             ]

dtXPConfig  :: XPConfig
dtXPConfig  = def
      { font                         = myFont
      , bgColor                      = "#282c34"
      , fgColor                      = "#bbc2cf"
      , bgHLight                     = "#c792ea"
      , fgHLight                     = "#000000"
      , borderColor                  = "#535974"
      , promptBorderWidth            = 0
      , promptKeymap                 = dtXPKeymap
      , position                     = Top
      , height                       = 26
      , historySize                  = 512
      , historyFilter                = id
      , defaultText                  = []
      , autoComplete                 = Just 100000  -- set Just 100000 for .1 sec
      , showCompletionOnTab          = False
      , searchPredicate              = fuzzyMatch
      , defaultPrompter              = id $ map toUpper  -- change prompt to UPPER
      , alwaysHighlight              = True
      , maxComplRows                 = Nothing      -- set to 'Just 5' for 5 rows
      }

-- The same config above minus the autocomplete feature which is annoying
-- on certain Xprompts, like the search engine prompts.
dtXPConfig' :: XPConfig
dtXPConfig' = dtXPConfig
      { autoComplete = Nothing
      }

-- A list of all of the standard Xmonad prompts and a key press assigned to them.
-- These are used in conjunction with keybinding I set later in the config.
promptList :: [(String, XPConfig -> X ())]
promptList =  [ ("m", manPrompt)          -- manpages prompt
              , ("p", passPrompt)         -- get passwords (requires 'pass')
              , ("g", passGeneratePrompt) -- generate passwords (requires 'pass')
              , ("r", passRemovePrompt)   -- remove passwords (requires 'pass')
              , ("s", sshPrompt)          -- ssh prompt
              , ("x", xmonadPrompt)       -- xmonad prompt
              ]

-- Same as the above list except this is for my custom prompts.
promptList' :: [(String, XPConfig -> String -> X (), String)]
promptList' =  [ ("c", calcPrompt, "qalc")         -- requires qalculate-gtk
               ]

calcPrompt c ans =
    inputPrompt c (trim ans) ?+ \input ->
        liftIO(runProcessWithInput "qalc" [input] "") >>= calcPrompt c
    where
        trim  = f . f
            where f = reverse . dropWhile isSpace

dtXPKeymap :: M.Map (KeyMask,KeySym) (XP ())
dtXPKeymap = M.fromList $
     map (first $ (,) controlMask)   -- control + <key>
     [ (xK_z, killBefore)            -- kill line backwards
     , (xK_k, killAfter)             -- kill line forwards
     , (xK_a, startOfLine)           -- move to the beginning of the line
     , (xK_e, endOfLine)             -- move to the end of the line
     , (xK_m, deleteString Next)     -- delete a character foward
     , (xK_b, moveCursor Prev)       -- move cursor forward
     , (xK_f, moveCursor Next)       -- move cursor backward
     , (xK_BackSpace, killWord Prev) -- kill the previous word
     , (xK_y, pasteString)           -- paste a string
     , (xK_g, quit)                  -- quit out of prompt
     , (xK_bracketleft, quit)
     ]
     ++
     map (first $ (,) altMask)       -- meta key + <key>
     [ (xK_BackSpace, killWord Prev) -- kill the prev word
     , (xK_f, moveWord Next)         -- move a word forward
     , (xK_b, moveWord Prev)         -- move a word backward
     , (xK_d, killWord Next)         -- kill the next word
     , (xK_n, moveHistory W.focusUp')   -- move up thru history
     , (xK_p, moveHistory W.focusDown') -- move down thru history
     ]
     ++
     map (first $ (,) 0) -- <key>
     [ (xK_Return, setSuccess True >> setDone True)
     , (xK_KP_Enter, setSuccess True >> setDone True)
     , (xK_BackSpace, deleteString Prev)
     , (xK_Delete, deleteString Next)
     , (xK_Left, moveCursor Prev)
     , (xK_Right, moveCursor Next)
     , (xK_Home, startOfLine)
     , (xK_End, endOfLine)
     , (xK_Down, moveHistory W.focusUp')
     , (xK_Up, moveHistory W.focusDown')
     , (xK_Escape, quit)
     ]

archwiki, ebay, news, reddit, urban :: S.SearchEngine

archwiki = S.searchEngine "archwiki" "https://wiki.archlinux.org/index.php?search="
ebay     = S.searchEngine "ebay" "https://www.ebay.com/sch/i.html?_nkw="
news     = S.searchEngine "news" "https://news.google.com/search?q="
reddit   = S.searchEngine "reddit" "https://www.reddit.com/search/?q="
urban    = S.searchEngine "urban" "https://www.urbandictionary.com/define.php?term="

-- This is the list of search engines that I want to use. Some are from
-- XMonad.Actions.Search, and some are the ones that I added above.
searchList :: [(String, S.SearchEngine)]
searchList = [ ("a", archwiki)
             , ("d", S.duckduckgo)
             , ("e", ebay)
             , ("g", S.google)
             , ("h", S.hoogle)
             , ("i", S.images)
             , ("n", news)
             , ("r", reddit)
             , ("s", S.stackage)
             , ("t", S.thesaurus)
             , ("v", S.vocabulary)
             , ("b", S.wayback)
             , ("u", urban)
             , ("w", S.wikipedia)
             , ("y", S.youtube)
             , ("z", S.amazon)
             ]

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "mocp" spawnMocp findMocp manageMocp
                ]
  where
    spawnTerm  = myTerminal ++ " -n scratchpad"
    findTerm   = resource =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnMocp  = myTerminal ++ " -n mocp 'mocp'"
    findMocp   = resource =? "mocp"
    manageMocp = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w

mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True
tall     = renamed [Replace "tall"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ limitWindows 12
           $ mySpacing 2
           $ ResizableTall 1 (3/100) (1/2) []
magnify  = renamed [Replace "magnify"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ magnifier
           $ limitWindows 12
           $ mySpacing 2
           $ ResizableTall 1 (3/100) (1/2) []
floats   = renamed [Replace "floats"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ limitWindows 20 simplestFloat
spirals  = renamed [Replace "spirals"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ mySpacing 2
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ limitWindows 7
           $ mySpacing 2
           $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           $ tabbed shrinkText myTabTheme

myTabTheme = def { fontName            = myFont
                 , activeColor         = "#46d9ff"
                 , inactiveColor       = "#313846"
                 , activeBorderColor   = "#46d9ff"
                 , inactiveBorderColor = "#282c34"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Mononoki Nerd Font:bold:size=20:antialias=true:hinting=true"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }

-- The layout hook
myLayoutHook = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats $ hiddenWindows
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
     where
         myDefaultLayout =     spirals
                           ||| tall
                           ||| threeCol
                           ||| noBorders tabs
                           ||| magnify
                           ||| floats

xmobarEscape :: String -> String
xmobarEscape = concatMap doubleLts
  where
        doubleLts '<' = "<<"
        doubleLts x   = [x]

--"[\61899 ]", 
--"[\61723 ]", 
myWorkspaces :: [String]
myWorkspaces = clickable . map xmobarEscape
            $ ["[1:\59333 ]", "[2:\57899 ]", "[3:\62057 ]", "[4:\61448 ]", "[5:\61715 ]", "[6:\61441 ]", "[7:\61912 ]", "[8:\61878 ]", "[9:\61459 ]"]
  where
        clickable l = [ "<action=xdotool key super+" ++ show (n) ++ ">" ++ ws ++ "</action>" |
                      (i,ws) <- zip [1..9] l,
                      let n = i ]

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     [ className    =? "mpv"                              --> doShift   ( myWorkspaces !! 3 )
     , className    =? "vlc"                              --> doShift   ( myWorkspaces !! 3 )
     , className    =? "signal-desktop"                   --> doShift   ( myWorkspaces !! 6 )
     , className    =? "telegram-desktop"                 --> doShift   ( myWorkspaces !! 6 )
     , className    =? "Gimp"                             --> doShift   ( myWorkspaces !! 1 )
     , className    =? "VirtualBox Manager"               --> doShift   ( myWorkspaces !! 4 )
     , resource     =? "desktop_window"                   --> doIgnore
     , resource     =? "desktop_window"                   --> doFloat                             -- Float dialogue windows
     , className    =? "vlc"                              --> doFloat
     , className    =? "mpv"                              --> doFloat
     , (className   =? "brave" <&&> resource =? "Dialog") --> doFloat                             -- float Firefox Dialog
     ] <+> namedScratchpadManageHook myScratchPads

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
    where fadeAmount = 1.0

myKeys :: [(String, X ())]
myKeys =
    -- Xmonad
        [ ("M-C-r", spawn "xmonad --recompile")                             -- Recompiles xmonad
        , ("M-S-r", spawn "xmonad --restart")                               -- Restarts xmonad
        , ("M-S-q", io exitSuccess)                                         -- Quits xmonad
    -- Run Prompt
        , ("M-S-<Return>", shellPrompt dtXPConfig)                          -- Shell Prompt
    -- Useful programs to have a keybinding for launch
        , ("M-<Return>", spawn (myTerminal))
        , ("M-b", spawn (myBrowser ))
        , ("M-M1-h", spawn (myTerminal ++ " -e bashtop"))
    -- Kill Windows
        , ("M-c", kill1)                                                    -- Kill the currently focused client
        , ("M-S-c", killAll)                                                -- Kill all windows on current workspace
    -- Copy & Kill Windows
        , ("M-C-s", windows copyToAll)
        , ("M-C-a", killAllOtherCopies)
    -- Minimize & Restore Windows
        , ("M-M1-m", withFocused hideWindow)                                -- Minimize Window
        , ("M-M1-l", popOldestHiddenWindow)                                 -- Restore Oldest Hidden Window
        , ("M-M1-r", popNewestHiddenWindow)                                 -- Restore Newest Hidden Window
    -- Workspaces
        , ("M-.", nextScreen)  -- Switch focus to next monitor
        , ("M-,", prevScreen)  -- Switch focus to prev monitor
        , ("M-S-<KP_Add>", shiftTo Next nonNSP >> moveTo Next nonNSP)       -- Shifts focused window to next ws
        , ("M-S-<KP_Subtract>", shiftTo Prev nonNSP >> moveTo Prev nonNSP)  -- Shifts focused window to prev ws
    -- Floating windows
        , ("M-f", sendMessage (T.Toggle "floats"))                          -- Toggles my 'floats' layout
        , ("M-t", withFocused $ windows . W.sink)                           -- Push floating window back to tile
        , ("M-S-t", sinkAll)                                                -- Push ALL floating windows to tile
    -- Increase/decrease spacing (gaps)
        , ("M-d", decWindowSpacing 4)                                       -- Decrease window spacing
        , ("M-i", incWindowSpacing 4)                                       -- Increase window spacing
        , ("M-S-d", decScreenSpacing 4)                                     -- Decrease screen spacing
        , ("M-S-i", incScreenSpacing 4)                                     -- Increase screen spacing
    -- Grid Select (CTR-g followed by a key)
        , ("C-g g", spawnSelected' myAppGrid)                               -- grid select favorite apps
        , ("C-g t", goToSelected $ mygridConfig myColorizer)                -- goto selected window
        , ("C-g b", bringSelected $ mygridConfig myColorizer)               -- bring selected window
    -- Windows navigation
        , ("M-m", windows W.focusMaster)                                    -- Move focus to the master window
        , ("M-j", windows W.focusDown)                                      -- Move focus to the next window
        , ("M-k", windows W.focusUp)                                        -- Move focus to the prev window
        , ("M-S-m", windows W.swapMaster)                                   -- Swap the focused window and the master window
        , ("M-S-j", windows W.swapDown)                                     -- Swap focused window with next window
        , ("M-S-k", windows W.swapUp)                                       -- Swap focused window with prev window
        , ("M-<Backspace>", promote)                                        -- Moves focused window to master, others maintain order
        , ("M-S-<Tab>", rotSlavesDown)                                      -- Rotate all windows except master and keep focus in place
        , ("M-C-<Tab>", rotAllDown)                                         -- Rotate all the windows in the current stack
        , ("M1-<Tab>", nextWS)                                              -- (Alt+Tab) -- Shift to next WS
        , ("M1-S-<Tab>", prevWS)                                            -- (Alt+Ctrl+Tab) -- Shift to previous WS
    -- Layouts
        , ("M-<Tab>", sendMessage NextLayout)                               -- Switch to next layout
        , ("M-C-M1-<Up>", sendMessage Arrange)
        , ("M-C-M1-<Down>", sendMessage DeArrange)
        , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
        , ("M-S-<Space>", sendMessage ToggleStruts)                         -- Toggles struts
        , ("M-S-n", sendMessage $ MT.Toggle NOBORDERS)                      -- Toggles noborder
    -- Increase/decrease windows in the master pane or the stack
        , ("M-S-<Up>", sendMessage (IncMasterN 1))                          -- Increase number of clients in master pane
        , ("M-S-<Down>", sendMessage (IncMasterN (-1)))                     -- Decrease number of clients in master pane
        , ("M-C-<Up>", increaseLimit)                                       -- Increase number of windows
        , ("M-C-<Down>", decreaseLimit)                                     -- Decrease number of windows
    -- Window resizing
        , ("M-h", sendMessage Shrink)                                       -- Shrink horiz window width
        , ("M-l", sendMessage Expand)                                       -- Expand horiz window width
        , ("M-M1-j", sendMessage MirrorShrink)                              -- Shrink vert window width
        , ("M-M1-k", sendMessage MirrorExpand)                              -- Exoand vert window width
    -- Scratchpads
        , ("M-C-<Return>", namedScratchpadAction myScratchPads "terminal")
        , ("M-C-c", namedScratchpadAction myScratchPads "mocp")
    -- Controls for mocp music player (SUPER-u followed by a key)
        , ("M-u p", spawn "mocp --play")
        , ("M-u l", spawn "mocp --next")
        , ("M-u h", spawn "mocp --previous")
        , ("M-u <Space>", spawn "mocp --toggle-pause")
        -- Multimedia Keys
        , ("<XF86AudioPlay>", spawn (myTerminal ++ "mocp --play"))
        , ("<XF86AudioPrev>", spawn (myTerminal ++ "mocp --previous"))
        , ("<XF86AudioNext>", spawn (myTerminal ++ "mocp --next"))
        -- , ("<XF86AudioMute>",   spawn "amixer set Master toggle")  -- Bug prevents it from toggling correctly in 12.04.
        , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
        , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
        , ("<XF86HomePage>", spawn "firefox")
        , ("<XF86Search>", safeSpawn "firefox" ["https://www.duckduckgo.com/"])
        ]
    -- Look at "search engines" section of this config for values for "k".
        ++ [("M-s " ++ k, S.promptSearch dtXPConfig' f) | (k,f) <- searchList ]
        ++ [("M-S-s " ++ k, S.selectSearch f) | (k,f) <- searchList ]
    -- Appending some extra xprompts to keybindings list.
    -- Look at "xprompt settings" section this of config for values for "k".
        ++ [("M-p " ++ k, f dtXPConfig') | (k,f) <- promptList ]
        ++ [("M-p " ++ k, f dtXPConfig' g) | (k,f,g) <- promptList' ]
    -- The following lines are needed for named scratchpads.
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "nsp"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "nsp"))

main :: IO ()
main = do
    xmproc <- spawnPipe "xmobar /home/m3w/.xmobarrc"
    xmonad $ ewmh def
        { manageHook = ( isFullscreen --> doFullFloat ) <+> myManageHook <+> manageDocks
        , handleEventHook    = serverModeEventHookCmd
                               <+> serverModeEventHook
                               <+> serverModeEventHookF "XMONAD_PRINT" (io . putStrLn)
                               <+> docksEventHook
        , modMask            = myModMask
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , layoutHook         = showWName' myShowWNameTheme $ myLayoutHook
        , workspaces         = myWorkspaces
        , borderWidth        = myBorderWidth
        , normalBorderColor  = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
        , logHook = workspaceHistoryHook <+> myLogHook <+> dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppCurrent = xmobarColor "#faff00" "" . wrap "{" "}" -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#00dfff" ""                -- Visible but not current workspace
                        , ppHidden = xmobarColor "#00ff85" "" . wrap "∙" ""   -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#ff859f" ""        -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor "#b3ddff" "" . shorten 40     -- Title of active window in xmobar
                        , ppSep =  "<fc=#0c020d> <fn=1>:</fn> </fc>"          -- Separators in xmobar
                        , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"  -- Urgent workspace
                        , ppExtras  = [windowCount]                           -- # of windows current workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                        }
        } `additionalKeysP` myKeys

