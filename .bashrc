#
# ~/.bashrc
#
export HOME=/home/m3w/

PATH="$HOME/.local/bin${PATH:+:${PATH}}"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

exec zsh
