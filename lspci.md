00:00.0 Host bridge: Intel Corporation 3rd Gen Core processor DRAM Controller (rev 09)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: ivb_uncore
00:01.0 PCI bridge: Intel Corporation Xeon E3-1200 v2/3rd Gen Core processor PCI Express Root Port (rev 09)
	Kernel driver in use: pcieport
00:02.0 VGA compatible controller: Intel Corporation 3rd Gen Core processor Graphics Controller (rev 09)
	DeviceName:  Onboard IGD
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10d7
	Kernel driver in use: i915
	Kernel modules: i915
00:14.0 USB controller: Intel Corporation 7 Series/C210 Series Chipset Family USB xHCI Host Controller (rev 04)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: xhci_hcd
	Kernel modules: xhci_pci
00:16.0 Communication controller: Intel Corporation 7 Series/C216 Chipset Family MEI Controller #1 (rev 04)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: mei_me
	Kernel modules: mei_me
00:1a.0 USB controller: Intel Corporation 7 Series/C216 Chipset Family USB Enhanced Host Controller #2 (rev 04)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: ehci-pci
	Kernel modules: ehci_pci
00:1b.0 Audio device: Intel Corporation 7 Series/C216 Chipset Family High Definition Audio Controller (rev 04)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: snd_hda_intel
	Kernel modules: snd_hda_intel
00:1c.0 PCI bridge: Intel Corporation 7 Series/C216 Chipset Family PCI Express Root Port 1 (rev c4)
	Kernel driver in use: pcieport
00:1c.1 PCI bridge: Intel Corporation 7 Series/C210 Series Chipset Family PCI Express Root Port 2 (rev c4)
	Kernel driver in use: pcieport
00:1c.2 PCI bridge: Intel Corporation 7 Series/C210 Series Chipset Family PCI Express Root Port 3 (rev c4)
	Kernel driver in use: pcieport
00:1c.3 PCI bridge: Intel Corporation 7 Series/C216 Chipset Family PCI Express Root Port 4 (rev c4)
	Kernel driver in use: pcieport
00:1d.0 USB controller: Intel Corporation 7 Series/C216 Chipset Family USB Enhanced Host Controller #1 (rev 04)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: ehci-pci
	Kernel modules: ehci_pci
00:1f.0 ISA bridge: Intel Corporation HM76 Express Chipset LPC Controller (rev 04)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: lpc_ich
	Kernel modules: lpc_ich
00:1f.2 SATA controller: Intel Corporation 7 Series Chipset Family 6-port SATA Controller [AHCI mode] (rev 04)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: ahci
00:1f.3 SMBus: Intel Corporation 7 Series/C216 Chipset Family SMBus Controller (rev 04)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: i801_smbus
	Kernel modules: i2c_i801

*****************************************************************************************************************

01:00.0 VGA compatible controller: NVIDIA Corporation GK107M [GeForce GTX 660M] (rev a1)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10d7
	Kernel driver in use: nvidia
	Kernel modules: nouveau, nvidia_drm, nvidia
03:00.0 Ethernet controller: Qualcomm Atheros Killer E220x Gigabit Ethernet Controller (rev 13)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: alx
	Kernel modules: alx
04:00.0 Unassigned class [ff00]: Realtek Semiconductor Co., Ltd. RTS5209 PCI Express Card Reader (rev 01)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: rtsx_pci
	Kernel modules: rtsx_pci
04:00.1 SD Host controller: Realtek Semiconductor Co., Ltd. RTS5209 PCI Express Card Reader (rev 01)
	Subsystem: Micro-Star International Co., Ltd. [MSI] Device 10c7
	Kernel driver in use: sdhci-pci
	Kernel modules: sdhci_pci
05:00.0 Network controller: Intel Corporation Centrino Wireless-N 135 (rev c4)
	Subsystem: Intel Corporation Centrino Wireless-N 135 BGN
	Kernel driver in use: iwlwifi
	Kernel modules: iwlwifi
