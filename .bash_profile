#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

[[ $(fgconsole >> /dev/null) == 1 ]] && exec startx --vt1

# [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
