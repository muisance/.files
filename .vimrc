let mapleader=","

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-surround'
Plug 'lervag/vimtex'
Plug 'w0rp/ale'
Plug 'xuhdev/vim-latex-live-preview'
Plug 'flazz/vim-colorschemes'
Plug 'chun-yang/auto-pairs'
Plug 'dpelle/vim-languagetool'
Plug 'junegunn/vim-easy-align'
Plug 'https://github.com/junegunn/vim-github-dashboard'
Plug 'arthurgorgonio/vim-themes-improved'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-markdown'
Plug 'ap/vim-css-color'
call plug#end()

"colorscheme lizard256
colorscheme deus

" Vim's auto indentation feature does not work properly with text copied from outside of Vim. Press the <F2> key to toggle paste mode on/off.
nnoremap <F2> :set invpaste paste?<CR>
imap <F2> <C-O>:set invpaste paste?<CR>
set pastetoggle=<F2>
syntax enable
set syn=vim
set tgc
set expandtab
set nu
set rnu
set sft
set cst
set acd
set ic
set cul
set csre
set title
set icon
set sc
set mod
set sw=4
set sts=4
set showmatch
set mouse=a
set et
set nowrap

let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='ayu_dark'

inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
