[0;1mName            :[0m abletonlink
[0;1mVersion         :[0m 3.0.2-3
[0;1mDescription     :[0m Synchronizes musical beat, tempo, and phase across multiple applications
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/ableton/link
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m asio
[0;1mOptional Deps   :[0m jack: for JACK examples [installed]
                  portaudio: for portaudio based examples [installed]
                  qt5-quickcontrols: for Qt examples
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m link
[0;1mInstalled Size  :[0m 1873.50 KiB
[0;1mPackager        :[0m David Runge <dave@sleepmap.de>
[0;1mBuild Date      :[0m Sun 22 Mar 2020 05:32:50 PM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:40:21 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m alsa-firmware
[0;1mVersion         :[0m 1.2.4-2
[0;1mDescription     :[0m Firmware binaries for loader programs in alsa-tools and hotplug firmware loader
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://alsa-project.org/
[0;1mLicenses        :[0m BSD  GPL2  LGPL2.1  custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 14.12 MiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Wed 21 Oct 2020 05:57:08 PM MSK
[0;1mInstall Date    :[0m Mon 09 Nov 2020 03:50:09 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m alsa-oss
[0;1mVersion         :[0m 1.1.8-3
[0;1mDescription     :[0m OSS compatibility library
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.alsa-project.org
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m libaoss.so=0-64  libalsatoss.so=0-64
[0;1mDepends On      :[0m glibc  libasound.so=2-64
[0;1mOptional Deps   :[0m bash: for aoss [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 84.55 KiB
[0;1mPackager        :[0m David Runge <dave@sleepmap.de>
[0;1mBuild Date      :[0m Fri 21 Feb 2020 03:03:03 AM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 12:08:23 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m alsa-plugins
[0;1mVersion         :[0m 1:1.2.2-2
[0;1mDescription     :[0m Additional ALSA plugins
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.alsa-project.org
[0;1mLicenses        :[0m LGPL2.1
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  libasound.so=2-64
[0;1mOptional Deps   :[0m dbus: for maemo plugin [installed]
                  jack: for pcm_jack plugin [installed]
                  libavtp: for pcm_aaf plugin
                  libsamplerate: for rate_samplerate plugin [installed]
                  libpulse: for conf_pulse, ctl_pulse and pcm_pulse plugins [installed]
                  speexdsp: for pcm_speex and rate_speexrate plugins [installed]
[0;1mRequired By     :[0m pulseaudio-alsa
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 346.73 KiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
[0;1mBuild Date      :[0m Wed 13 May 2020 01:16:12 PM MSK
[0;1mInstall Date    :[0m Mon 09 Nov 2020 03:50:09 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m alsa-utils
[0;1mVersion         :[0m 1.2.4-2
[0;1mDescription     :[0m Advanced Linux Sound Architecture - Utilities
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.alsa-project.org
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  pciutils  psmisc  libasound.so=2-64  libatopology.so=2-64  libformw.so=6-64  libmenuw.so=6-64  libncursesw.so=6-64  libpanelw.so=6-64  libsamplerate.so=0-64
[0;1mOptional Deps   :[0m fftw: for alsabat [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.08 MiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Wed 21 Oct 2020 06:06:56 PM MSK
[0;1mInstall Date    :[0m Thu 12 Nov 2020 11:36:43 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m apcupsd
[0;1mVersion         :[0m 3.14.14-6
[0;1mDescription     :[0m Power mangement and controlling most of APC's UPS models
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://www.apcupsd.org
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gcc-libs  systemd-sysvcompat  libusb-compat
[0;1mOptional Deps   :[0m gd: for CGI
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m netdata
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 920.11 KiB
[0;1mPackager        :[0m Florian Pritz <bluewind@xinu.at>
[0;1mBuild Date      :[0m Sun 30 Aug 2020 12:41:31 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 05:57:33 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m apparmor
[0;1mVersion         :[0m 3.0.1-1
[0;1mDescription     :[0m Mandatory Access Control (MAC) using Linux Security Module (LSM)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://gitlab.com/apparmor/apparmor
[0;1mLicenses        :[0m GPL2  LGPL2.1
[0;1mGroups          :[0m None
[0;1mProvides        :[0m libapparmor.so=1-64
[0;1mDepends On      :[0m audit  glibc  pam  python  libcrypt.so=2-64
[0;1mOptional Deps   :[0m perl: for perl bindings [installed]
                  python-notify2: for aa-notify
                  python-psutil: for aa-notify [installed]
                  ruby: for ruby bindings [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m torbrowser-launcher
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.94 MiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Tue 08 Dec 2020 12:43:02 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:12:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m appstream
[0;1mVersion         :[0m 0.13.1-1
[0;1mDescription     :[0m Provides a standard for creating app stores across distributions
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://distributions.freedesktop.org/wiki/AppStream
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libyaml  libxml2  libstemmer  glib2  lmdb  libsoup
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m appstream-generator
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 6.24 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Tue 01 Dec 2020 10:17:19 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:12:41 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m appstream-generator
[0;1mVersion         :[0m 0.8.2-5
[0;1mDescription     :[0m A fast AppStream metadata generator
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/ximion/appstream-generator
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m appstream  libarchive  librsvg  optipng  glibd
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 4.08 MiB
[0;1mPackager        :[0m Dan Printzell <arch@vild.io>
[0;1mBuild Date      :[0m Wed 11 Nov 2020 10:57:52 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 05:03:08 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m appstream-glib
[0;1mVersion         :[0m 0.7.18-1
[0;1mDescription     :[0m Objects and methods for reading and writing AppStream metadata
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://people.freedesktop.org/~hughsient/appstream-glib/
[0;1mLicenses        :[0m LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m appdata-tools  libappstream-glib.so=8-64
[0;1mDepends On      :[0m gtk3  libyaml  pacman  gcab  libsoup  libstemmer
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m appdata-tools
[0;1mReplaces        :[0m appdata-tools
[0;1mInstalled Size  :[0m 3.59 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Tue 08 Sep 2020 06:28:57 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 05:03:08 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m apr
[0;1mVersion         :[0m 1.7.0-3
[0;1mDescription     :[0m The Apache Portable Runtime
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://apr.apache.org/
[0;1mLicenses        :[0m APACHE
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m util-linux
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1177.68 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 05:04:59 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 10:21:23 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m arch-wiki-docs
[0;1mVersion         :[0m 20200527-1
[0;1mDescription     :[0m Pages from Arch Wiki optimized for offline browsing
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/lahwaacz/arch-wiki-docs
[0;1mLicenses        :[0m FDL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 129.59 MiB
[0;1mPackager        :[0m Sergej Pupykin <pupykin.s+arch@gmail.com>
[0;1mBuild Date      :[0m Wed 27 May 2020 10:49:44 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:15:16 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m archlinux-appstream-data
[0;1mVersion         :[0m 20201128-1
[0;1mDescription     :[0m Arch Linux application database for AppStream-based software centers
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.archlinux.org
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 18.97 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Sat 28 Nov 2020 10:34:36 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:12:42 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m archlinux-menus
[0;1mVersion         :[0m 2.5-5
[0;1mDescription     :[0m Arch Linux specific XDG-compliant menu
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.archlinux.org/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xdg-utils
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 17.82 KiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Fri 03 Jan 2020 12:59:06 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:00:01 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m archlinux-xdg-menu
[0;1mVersion         :[0m 0.7.6.3-2
[0;1mDescription     :[0m automatic generate WM menu from xdg files
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://wiki.archlinux.org/index.php/XdgMenu
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m perl  perl-xml-parser
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 135.43 KiB
[0;1mPackager        :[0m Jelle van der Waa <jelle@archlinux.org>
[0;1mBuild Date      :[0m Mon 06 Jan 2020 06:51:24 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:00:01 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m asio
[0;1mVersion         :[0m 1.18.0-1
[0;1mDescription     :[0m Cross-platform C++ library for ASynchronous network I/O
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://think-async.com/Asio/
[0;1mLicenses        :[0m custom:boost
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m abletonlink
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 4.09 MiB
[0;1mPackager        :[0m Levente Polyak <anthraxx@archlinux.org>
[0;1mBuild Date      :[0m Sun 27 Sep 2020 03:26:49 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 06:21:01 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m atom
[0;1mVersion         :[0m 1.53.0-1
[0;1mDescription     :[0m A hackable text editor for the 21st Century
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/atom/atom
[0;1mLicenses        :[0m MIT  custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m apm  electron6  libxkbfile  ripgrep
[0;1mOptional Deps   :[0m ctags: symbol indexing support
                  git: Git and GitHub integration [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m atom-editor
[0;1mInstalled Size  :[0m 256.75 MiB
[0;1mPackager        :[0m Nicola Squartini <tensor5@gmail.com>
[0;1mBuild Date      :[0m Wed 11 Nov 2020 03:41:28 AM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 04:46:31 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m autoconf
[0;1mVersion         :[0m 2.69-7
[0;1mDescription     :[0m A GNU tool for automatically configuring source code
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.gnu.org/software/autoconf
[0;1mLicenses        :[0m GPL2  GPL3  custom
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m awk  m4  diffutils  perl  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2047.05 KiB
[0;1mPackager        :[0m Lukas Fleischer <lfleischer@archlinux.org>
[0;1mBuild Date      :[0m Sun 09 Feb 2020 09:26:33 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m automake
[0;1mVersion         :[0m 1.16.2-3
[0;1mDescription     :[0m A GNU tool for automatically creating Makefiles
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.gnu.org/software/automake
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m perl  bash
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1590.70 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Sat 06 Jun 2020 11:03:04 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m base
[0;1mVersion         :[0m 2-2
[0;1mDescription     :[0m Minimal package set to define a basic Arch Linux installation
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.archlinux.org
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m filesystem  gcc-libs  glibc  bash  coreutils  file  findutils  gawk  grep  procps-ng  sed  tar  gettext  pciutils  psmisc  shadow  util-linux  bzip2  gzip  xz  licenses  pacman  systemd  systemd-sysvcompat  iputils  iproute2
[0;1mOptional Deps   :[0m linux: bare metal support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 0.00 B
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Wed 13 Nov 2019 07:21:49 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m bash-completion
[0;1mVersion         :[0m 2.11-1
[0;1mDescription     :[0m Programmable completion for the bash shell
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/scop/bash-completion
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bash
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m bash
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 851.13 KiB
[0;1mPackager        :[0m Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
[0;1mBuild Date      :[0m Sun 09 Aug 2020 07:37:57 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:00:27 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m bashtop
[0;1mVersion         :[0m 0.9.25-1
[0;1mDescription     :[0m Linux resource monitor
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/aristocratos/bashtop
[0;1mLicenses        :[0m Apache
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bash  coreutils  grep  sed  gawk  procps-ng
[0;1mOptional Deps   :[0m curl: themes download [installed]
                  lm_sensors: cpu temperatures [installed]
                  sysstat: disk read/write stats
                  python-psutil: use python for data collection [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 201.65 KiB
[0;1mPackager        :[0m Sébastien Luttringer <seblu@seblu.net>
[0;1mBuild Date      :[0m Fri 21 Aug 2020 11:53:14 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 08:36:27 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m binutils
[0;1mVersion         :[0m 2.35.1-1
[0;1mDescription     :[0m A set of programs to assemble and manipulate binary and object files
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/binutils/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  zlib  elfutils
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m gcc
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m binutils-multilib
[0;1mReplaces        :[0m binutils-multilib
[0;1mInstalled Size  :[0m 30.20 MiB
[0;1mPackager        :[0m Jelle van der Waa <jelle@archlinux.org>
[0;1mBuild Date      :[0m Wed 14 Oct 2020 06:58:41 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m bison
[0;1mVersion         :[0m 3.7.2-1
[0;1mDescription     :[0m The GNU general-purpose parser generator
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/bison/bison.html
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  m4  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.35 MiB
[0;1mPackager        :[0m Lukas Fleischer <lfleischer@archlinux.org>
[0;1mBuild Date      :[0m Sun 13 Sep 2020 03:49:53 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m bleachbit
[0;1mVersion         :[0m 4.0.0-4
[0;1mDescription     :[0m Deletes unneeded files to free disk space and maintain privacy
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.bleachbit.org/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python-gobject  gtk3
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.80 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Thu 12 Nov 2020 10:39:51 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:12:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m boost
[0;1mVersion         :[0m 1.74.0-2
[0;1mDescription     :[0m Free peer-reviewed portable C++ source libraries - development headers
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.boost.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m boost-libs=1.74.0
[0;1mOptional Deps   :[0m python: for python bindings [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 159.98 MiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Wed 09 Dec 2020 10:42:03 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:01 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m brave-bin
[0;1mVersion         :[0m 1:1.16.76-1
[0;1mDescription     :[0m Web browser that blocks ads and trackers by default (binary release).
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://brave.com/download
[0;1mLicenses        :[0m MPL2  BSD  custom:chromium
[0;1mGroups          :[0m None
[0;1mProvides        :[0m brave  brave-browser
[0;1mDepends On      :[0m gtk3  nss  alsa-lib  libxss  ttf-font
[0;1mOptional Deps   :[0m cups: Printer support [installed]
                  pepper-flash: Adobe Flash support
                  libgnome-keyring: Enable GNOME keyring support
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m brave
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 238.88 MiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Thu 12 Nov 2020 11:44:52 PM MSK
[0;1mInstall Date    :[0m Thu 12 Nov 2020 11:50:34 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m bullet
[0;1mVersion         :[0m 3.07-1
[0;1mDescription     :[0m A 3D Collision Detection and Rigid Body Dynamics Library for games and animation
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://www.bulletphysics.com/Bullet/
[0;1mLicenses        :[0m custom:zlib
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m glu: for the example browser [installed]
                  python: python bindings [installed]
                  python-numpy: python bindings
                  bullet-docs: documentation
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 131.34 MiB
[0;1mPackager        :[0m Sven-Hendrik Haase <svenstaro@gmail.com>
[0;1mBuild Date      :[0m Fri 04 Dec 2020 10:30:44 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:09 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m cabal-install
[0;1mVersion         :[0m 3.2.0.0-119
[0;1mDescription     :[0m The command-line interface for Cabal and Hackage.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://hackage.haskell.org/package/cabal-install
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ghc-libs  haskell-async  haskell-base16-bytestring  haskell-cryptohash-sha256  haskell-echo  haskell-edit-distance  haskell-hackage-security  haskell-hashable  haskell-http  haskell-network  haskell-network-uri  haskell-random  haskell-resolv  haskell-tar  haskell-zlib  haskell-lukko
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 11.80 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Thu 10 Dec 2020 12:10:25 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:13 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m calcurse
[0;1mVersion         :[0m 4.7.0-1
[0;1mDescription     :[0m A text-based personal organizer.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://calcurse.org/
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ncurses
[0;1mOptional Deps   :[0m python: for CalDAV support [installed]
                  python-httplib2: for CalDAV support
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 860.25 KiB
[0;1mPackager        :[0m Lukas Fleischer <lfleischer@archlinux.org>
[0;1mBuild Date      :[0m Thu 15 Oct 2020 02:02:50 AM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 02:40:25 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m canto-curses
[0;1mVersion         :[0m 0.9.9-3
[0;1mDescription     :[0m Next-gen console RSS/Atom reader
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/themoken/canto-curses
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m canto-daemon  glibc  libncursesw.so=6-64  libreadline.so=8-64  python
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 589.56 KiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Thu 12 Nov 2020 08:10:46 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:13 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m chromium
[0;1mVersion         :[0m 87.0.4280.88-1
[0;1mDescription     :[0m A web browser built for speed, simplicity, and security
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.chromium.org/Home
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gtk3  nss  alsa-lib  xdg-utils  libxss  libcups  libgcrypt  ttf-liberation  systemd  dbus  libpulse  pciutils  json-glib  desktop-file-utils  hicolor-icon-theme  icu  libxml2  fontconfig  harfbuzz  libjpeg  libpng  re2  snappy  ffmpeg  flac  libwebp  minizip  libxslt  freetype2  opus
[0;1mOptional Deps   :[0m pepper-flash: support for Flash content
                  libpipewire02: WebRTC desktop sharing under Wayland
                  libva: hardware-accelerated video decode [experimental] [installed]
                  kdialog: needed for file dialogs in KDE
                  org.freedesktop.secrets: password storage backend on GNOME / Xfce
                  kwallet: for storing passwords in KWallet on KDE desktops [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 209.03 MiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Thu 03 Dec 2020 03:56:58 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:21 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m cmake
[0;1mVersion         :[0m 3.19.1-2
[0;1mDescription     :[0m A cross-platform open-source make system
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.cmake.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m curl  libarchive  shared-mime-info  jsoncpp  libjsoncpp.so=24-64  libuv  rhash
[0;1mOptional Deps   :[0m qt5-base: cmake-gui [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 45.00 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Wed 02 Dec 2020 07:47:32 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:23 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m cmus
[0;1mVersion         :[0m 2.8.0-5
[0;1mDescription     :[0m Feature-rich ncurses-based music player
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://cmus.github.io/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ncurses  libdiscid
[0;1mOptional Deps   :[0m alsa-lib: for ALSA output plugin support [installed]
                  libao: for AO output plugin support [installed]
                  libpulse: for PulseAudio output plugin support [installed]
                  faad2: for AAC input plugin support [installed]
                  ffmpeg: for ffmpeg input plugin support [installed]
                  flac: for flac input plugin support [installed]
                  jack: for jack plugin support [installed]
                  libmad: for mp3 input plugin support [installed]
                  libmodplug: for modplug input plugin support [installed]
                  libmp4v2: for mp4 input plugin support
                  libmpcdec: for musepack input plugin support [installed]
                  libsamplerate: for sampe rate converter support [installed]
                  libvorbis: for vorbis input plugin support [installed]
                  libcdio-paranoia: for cdio support [installed]
                  opusfile: for opus input plugin support
                  wavpack: for wavpack input plugin support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 794.61 KiB
[0;1mPackager        :[0m Alexander Rødseth <rodseth@gmail.com>
[0;1mBuild Date      :[0m Sun 16 Aug 2020 06:19:38 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 10:23:09 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m conky
[0;1mVersion         :[0m 1.11.6-1
[0;1mDescription     :[0m Lightweight system monitor for X
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/brndnmtthws/conky
[0;1mLicenses        :[0m BSD  GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glib2  lua  wireless_tools  libxdamage  libxinerama  libxft  imlib2  libxml2  libpulse  libxnvctrl
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 4.67 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sun 22 Nov 2020 03:35:02 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:22:50 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m connman
[0;1mVersion         :[0m 1.38-1
[0;1mDescription     :[0m Intel's modular network connection manager
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://01.org/connman
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m dbus  iptables  gnutls  glib2
[0;1mOptional Deps   :[0m bluez: Support for Bluetooth devices
                  wpa_supplicant: for WiFi devices [installed]
                  pptpclient: for ppp support [installed]
                  openvpn: for VPN Support [installed]
                  iwd: for WiFi devices [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1752.97 KiB
[0;1mPackager        :[0m Christian Rebischke <Chris.Rebischke@archlinux.org>
[0;1mBuild Date      :[0m Sat 22 Feb 2020 04:16:09 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 02:42:56 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m csound
[0;1mVersion         :[0m 6.15.0-2
[0;1mDescription     :[0m A programming language for sound rendering and signal processing.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://csound.com
[0;1mLicenses        :[0m LGPL2.1
[0;1mGroups          :[0m pro-audio
[0;1mProvides        :[0m libcsound64.so=6.0-64  libcsnd6.so=6.0-64
[0;1mDepends On      :[0m gcc-libs  glibc  libcurl.so=4-64  libsamplerate.so=0-64  libsndfile.so=1-64
[0;1mOptional Deps   :[0m alsa-lib: for librtalsa plugin [installed]
                  csound-doc: for the canonical Csound Reference Manual
                  csoundqt: for frontend/ editor with integrated help
                  fltk: for libvirtual and libwidgets plugins
                  hdf5: for hdf5ops plugin
                  java-runtime: java integration
                  faust: for libfaustcsound plugin
                  fluidsynth: for libfluidOpcodes plugin [installed]
                  jack: for libjacko, librtjack and libjackTransport plugins [installed]
                  liblo: for libosc plugin
                  ncurses: for libfaustcsound plugin [installed]
                  libpng: for libimage plugin [installed]
                  portmidi: for libpmidi plugin
                  portaudio: for librtpa plugin [installed]
                  libpulse: for librtpulse plugin [installed]
                  stk: for libstkops plugin
                  libwebsockets: for libwebsocketIO plugin
                  wiiuse: for libwiimote plugin
                  libx11: for libwidgets plugin [installed]
                  lua: for LUA integration [installed]
                  openssl: for libfaustcsound plugin [installed]
                  vim-csound: vim integration
                  zlib: for libfaustcsound plugin [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 12.90 MiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Mon 07 Sep 2020 11:03:03 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 03:41:50 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m cups
[0;1mVersion         :[0m 1:2.3.3op1-1
[0;1mDescription     :[0m The CUPS Printing System - daemon package
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/OpenPrinting/cups
[0;1mLicenses        :[0m Apache  custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m acl  pam  libcups>=2.3.3op1  cups-filters  bc  dbus  systemd  libpaper  hicolor-icon-theme
[0;1mOptional Deps   :[0m ipp-usb: allows to send HTTP requests via a USB connection on devices without Ethernet or WiFi connections
                  xdg-utils: xdg .desktop file support [installed]
                  colord: for ICC color profile support [installed]
                  logrotate: for logfile rotation support
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m brave-bin
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 12.32 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Fri 27 Nov 2020 11:01:44 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:23:00 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m deadbeef
[0;1mVersion         :[0m 1.8.4-1
[0;1mDescription     :[0m A GTK+ audio player for GNU/Linux.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://deadbeef.sourceforge.net
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m alsa-lib  hicolor-icon-theme  desktop-file-utils  jansson
[0;1mOptional Deps   :[0m gtk2: for the GTK2 interface [installed]
                  gtk3: for the GTK3 interface [installed]
                  libsamplerate: for Resampler plugin [installed]
                  libvorbis: for Ogg Vorbis playback [installed]
                  libmad: for MP1/MP2/MP3 playback [installed]
                  mpg123: for MP1/MP2/MP3 playback [installed]
                  flac: for FLAC playback [installed]
                  curl: for Last.fm scrobbler, SHOUTcast, Icecast, Podcast support [installed]
                  imlib2: for artwork plugin [installed]
                  wavpack: for WavPack playback [installed]
                  libsndfile: for Wave playback [installed]
                  libcdio: audio cd plugin [installed]
                  libcddb: audio cd plugin [installed]
                  faad2: for AAC/MP4 support [installed]
                  dbus: for OSD notifications support [installed]
                  pulseaudio: for PulseAudio output plugin [installed]
                  libx11: for global hotkeys plugin [installed]
                  zlib: for Audio Overload plugin [installed]
                  libzip: for vfs_zip plugin [installed]
                  ffmpeg: for ffmpeg plugin [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 11.63 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Wed 01 Jul 2020 07:58:49 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 04:49:57 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m dialog
[0;1mVersion         :[0m 1:1.3_20201126-1
[0;1mDescription     :[0m A tool to display dialog boxes from shell scripts
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://invisible-island.net/dialog/
[0;1mLicenses        :[0m LGPL2.1
[0;1mGroups          :[0m None
[0;1mProvides        :[0m libdialog.so=15-64
[0;1mDepends On      :[0m sh  ncurses
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m netctl
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 459.77 KiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Fri 27 Nov 2020 03:19:04 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:28 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m dietlibc
[0;1mVersion         :[0m 0.34-2
[0;1mDescription     :[0m a libc optimized for small size
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.fefe.de/dietlibc/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.48 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 08:11:42 PM MSK
[0;1mInstall Date    :[0m Mon 09 Nov 2020 04:11:24 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m dmenu-distrotube-git
[0;1mVersion         :[0m 4.9.r3.68906a3-1
[0;1mDescription     :[0m This is my personal build of dmenu that is patched for fonts, centering, borders, etc.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gitlab.com/dwt1/dmenu-distrotube.git
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m dmenu
[0;1mDepends On      :[0m ttf-hack  ttf-joypixels
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m dmenu
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 289.22 KiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sat 07 Nov 2020 01:16:02 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:19:59 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m docker
[0;1mVersion         :[0m 1:19.03.14-3
[0;1mDescription     :[0m Pack, ship and run any application as a lightweight container
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.docker.com/
[0;1mLicenses        :[0m Apache
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  bridge-utils  iproute2  device-mapper  sqlite  systemd-libs  libseccomp  libtool  runc  containerd
[0;1mOptional Deps   :[0m btrfs-progs: btrfs backend support [installed]
                  pigz: parallel gzip compressor support
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 246.02 MiB
[0;1mPackager        :[0m Morten Linderud <foxboron@archlinux.org>
[0;1mBuild Date      :[0m Sat 05 Dec 2020 07:35:26 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:34 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m dosfstools
[0;1mVersion         :[0m 4.1-3
[0;1mDescription     :[0m DOS filesystem utilities
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/dosfstools/dosfstools
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m libblockdev
[0;1mOptional For    :[0m grub  udisks2
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 177.94 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Wed 13 Nov 2019 07:12:22 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:20:38 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m dstat
[0;1mVersion         :[0m 0.7.4-5
[0;1mDescription     :[0m A versatile resource statistics tool
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m http://dag.wieers.com/home-made/dstat/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python  python-six
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 375.81 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 07:48:52 PM MSK
[0;1mInstall Date    :[0m Mon 07 Dec 2020 06:02:36 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m dunst
[0;1mVersion         :[0m 1.5.0-1
[0;1mDescription     :[0m Customizable and lightweight notification-daemon
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://dunst-project.org/
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m notification-daemon
[0;1mDepends On      :[0m libxinerama  libxss  pango  gdk-pixbuf2  libxrandr  glib2
[0;1mOptional Deps   :[0m libnotify: dunstify [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 171.11 KiB
[0;1mPackager        :[0m Daniel M. Capella <polyzen@archlinux.org>
[0;1mBuild Date      :[0m Tue 28 Jul 2020 10:53:19 PM MSK
[0;1mInstall Date    :[0m Mon 30 Nov 2020 12:44:54 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m dzen2
[0;1mVersion         :[0m 0.9.5.14.488ab66-2
[0;1mDescription     :[0m General purpose messaging, notification and menuing program for X11
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/robm/dzen
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxpm  libxinerama  libxft
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 141.62 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Mon 25 May 2020 04:21:28 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 03:09:47 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m efibootmgr
[0;1mVersion         :[0m 17-2
[0;1mDescription     :[0m Linux user-space application to modify the EFI Boot Manager
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/rhboot/efibootmgr
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  popt  libefiboot.so=1-64  libefivar.so=1-64
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m grub
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 78.05 KiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Sat 05 Sep 2020 12:06:05 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:20:38 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m fakeroot
[0;1mVersion         :[0m 1.25.3-1
[0;1mDescription     :[0m Tool for simulating superuser privileges
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://tracker.debian.org/pkg/fakeroot
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  filesystem  sed  util-linux  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 138.14 KiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Tue 27 Oct 2020 10:42:53 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m fcitx
[0;1mVersion         :[0m 4.2.9.8-1
[0;1mDescription     :[0m Flexible Context-aware Input Tool with eXtension
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://fcitx-im.org
[0;1mLicenses        :[0m GPL  LGPL
[0;1mGroups          :[0m fcitx-im
[0;1mProvides        :[0m fcitx-gtk2  fcitx-gtk3
[0;1mDepends On      :[0m pango  libxinerama  gtk-update-icon-cache  shared-mime-info  hicolor-icon-theme  desktop-file-utils  libxkbfile  libxfixes  dbus  icu  libxkbcommon
[0;1mOptional Deps   :[0m enchant: for word predication support [installed]
                  opencc: optional engine to do chinese convert
                  gettext: for fcitx-po-parser [installed]
                  fcitx-configtool: for configuration (GTK based) [installed]
                  kcm-fcitx: for configuration under KDE
[0;1mRequired By     :[0m fcitx-configtool  fcitx-qt5  fcitx-ui-light
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m fcitx-gtk2  fcitx-gtk3
[0;1mReplaces        :[0m fcitx-gtk2  fcitx-gtk3
[0;1mInstalled Size  :[0m 34.89 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 01 Aug 2020 07:47:58 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 06:10:14 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m fcitx-configtool
[0;1mVersion         :[0m 0.4.10-4
[0;1mDescription     :[0m GTK based config tool for Fcitx
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://fcitx.googlecode.com/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m fcitx>=4.2.7  gtk3  iso-codes
[0;1mOptional Deps   :[0m fcitx-qt4: for some configuration windows
                  fcitx-qt5: for some other configuration windows [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m fcitx
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 130.61 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Mon 25 May 2020 04:20:01 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 04:01:25 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m fcitx-qt5
[0;1mVersion         :[0m 1.2.5-3
[0;1mDescription     :[0m Qt5 IM Module for Fcitx
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/fcitx/fcitx-qt5
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m fcitx-im
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m fcitx  libxkbcommon  qt5-base
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m fcitx-configtool
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 703.30 KiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Thu 19 Nov 2020 10:47:44 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:23:14 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m fcitx-ui-light
[0;1mVersion         :[0m 0.1.3-5
[0;1mDescription     :[0m Light weight xlib and xft based ui for fcitx.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/fcitx/fcitx-ui-light
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m fcitx  libxpm
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 91.12 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 07:05:58 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 04:01:25 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m feh
[0;1mVersion         :[0m 3.6.1-1
[0;1mDescription     :[0m Fast and light imlib2-based image viewer
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://feh.finalrewind.org/
[0;1mLicenses        :[0m custom:MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m imlib2  curl  libxinerama  libexif
[0;1mOptional Deps   :[0m imagemagick: support more file formats [installed]
                  jpegexiforient: set exif rotation tag
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m neofetch
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 446.11 KiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Sun 06 Dec 2020 10:21:53 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:34 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m figlet
[0;1mVersion         :[0m 2.2.5-4
[0;1mDescription     :[0m A program for making large letters out of ordinary text
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://www.figlet.org/
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 641.95 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 16 May 2020 03:14:26 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:19:36 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m file
[0;1mVersion         :[0m 5.39-1
[0;1mDescription     :[0m File type identification utility
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.darwinsys.com/file/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m libmagic.so=1-64
[0;1mDepends On      :[0m glibc  zlib  xz  bzip2  libseccomp  libseccomp.so=2-64
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m base  moc  util-linux  xdg-utils  zathura
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 6.56 MiB
[0;1mPackager        :[0m Sébastien Luttringer <seblu@seblu.net>
[0;1mBuild Date      :[0m Tue 16 Jun 2020 07:04:54 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:43 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m findutils
[0;1mVersion         :[0m 4.7.0-2
[0;1mDescription     :[0m GNU utilities to locate files
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/findutils/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m base  ca-certificates-utils  mkinitcpio
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1619.28 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Tue 12 Nov 2019 03:37:08 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:43 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m firefox
[0;1mVersion         :[0m 83.0-2
[0;1mDescription     :[0m Standalone web browser from mozilla.org
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.mozilla.org/firefox/
[0;1mLicenses        :[0m MPL  GPL  LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gtk3  libxt  mime-types  dbus-glib  ffmpeg  nss  ttf-font  libpulse
[0;1mOptional Deps   :[0m networkmanager: Location detection via available WiFi networks [installed]
                  libnotify: Notification integration [installed]
                  pulseaudio: Audio support [installed]
                  speech-dispatcher: Text-to-Speech
                  hunspell-en_US: Spell checking, American English
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 206.90 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Thu 26 Nov 2020 07:52:57 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:23:23 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m fish
[0;1mVersion         :[0m 3.1.2-2
[0;1mDescription     :[0m Smart and user friendly shell intended mostly for interactive use
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://fishshell.com/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  gcc-libs  ncurses  pcre2
[0;1mOptional Deps   :[0m python: man page completion parser / web config tool [installed]
                  pkgfile: command-not-found hook
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m fzf
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 13.75 MiB
[0;1mPackager        :[0m Levente Polyak <anthraxx@archlinux.org>
[0;1mBuild Date      :[0m Sun 24 May 2020 12:58:39 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:26:44 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m flex
[0;1mVersion         :[0m 2.6.4-3
[0;1mDescription     :[0m A tool for generating text-scanning programs
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/westes/flex
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  m4  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 971.00 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Wed 13 Nov 2019 08:43:31 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:48 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m fontforge
[0;1mVersion         :[0m 20200314-5
[0;1mDescription     :[0m Outline and bitmap font editor
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://fontforge.github.io/
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libtool  pango  giflib  libtiff  libxml2  libspiro  python  potrace  woff2  gtk3  libuninameslist
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 29.22 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Thu 12 Nov 2020 10:53:47 PM MSK
[0;1mInstall Date    :[0m Wed 09 Dec 2020 11:28:42 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m fzf
[0;1mVersion         :[0m 0.24.4-1
[0;1mDescription     :[0m Command-line fuzzy finder
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/junegunn/fzf
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bash
[0;1mOptional Deps   :[0m fish: fish keybindings [installed]
                  tmux: fzf-tmux script for launching fzf in a tmux pane [installed]
                  vim: plugin [installed]
                  zsh: zsh keybindings [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.13 MiB
[0;1mPackager        :[0m Morten Linderud <foxboron@archlinux.org>
[0;1mBuild Date      :[0m Sun 06 Dec 2020 12:08:27 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:36 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gawk
[0;1mVersion         :[0m 5.1.0-1
[0;1mDescription     :[0m GNU version of awk
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/gawk/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m awk
[0;1mDepends On      :[0m sh  glibc  mpfr
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m autoconf  base  bashtop  mkinitcpio
[0;1mOptional For    :[0m vim-runtime
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.66 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Wed 15 Apr 2020 09:38:14 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:43 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gcc
[0;1mVersion         :[0m 10.2.0-4
[0;1mDescription     :[0m The GNU Compiler Collection - C and C++ frontends
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://gcc.gnu.org
[0;1mLicenses        :[0m GPL  LGPL  FDL  custom
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m gcc-multilib
[0;1mDepends On      :[0m gcc-libs=10.2.0-4  binutils>=2.28  libmpc
[0;1mOptional Deps   :[0m lib32-gcc-libs: for generating code for 32-bit ABI [installed]
[0;1mRequired By     :[0m ghc
[0;1mOptional For    :[0m xorg-xrdb
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m gcc-multilib
[0;1mInstalled Size  :[0m 147.32 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Mon 09 Nov 2020 02:43:35 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:13:36 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gettext
[0;1mVersion         :[0m 0.21-1
[0;1mDescription     :[0m GNU internationalization library
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/gettext/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gcc-libs  acl  sh  glib2  libunistring  libcroco
[0;1mOptional Deps   :[0m git: for autopoint infrastructure updates [installed]
[0;1mRequired By     :[0m base  exiv2  grub
[0;1mOptional For    :[0m fcitx
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 7.51 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Tue 04 Aug 2020 10:43:10 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:43 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gimp
[0;1mVersion         :[0m 2.10.22-1
[0;1mDescription     :[0m GNU Image Manipulation Program
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gimp.org/
[0;1mLicenses        :[0m GPL  LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m babl  dbus-glib  desktop-file-utils  gegl  glib-networking  hicolor-icon-theme  openjpeg2  lcms2  libheif  libexif  libgudev  libmng  libmypaint  librsvg  libwebp  libwmf  libxmu  libxpm  mypaint-brushes1  openexr  poppler-data  gtk2
[0;1mOptional Deps   :[0m gutenprint: for sophisticated printing only as gimp has built-in cups print support
                  poppler-glib: for pdf support [installed]
                  alsa-lib: for MIDI event controller module [installed]
                  curl: for URI support [installed]
                  ghostscript: for postscript support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m gimp-plugin-wavelet-decompose
[0;1mReplaces        :[0m gimp-plugin-wavelet-decompose
[0;1mInstalled Size  :[0m 111.34 MiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Sun 04 Oct 2020 10:15:11 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 03:00:07 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m git
[0;1mVersion         :[0m 2.29.2-1
[0;1mDescription     :[0m the fast distributed version control system
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://git-scm.com/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m curl  expat  perl  perl-error  perl-mailtools  openssl  pcre2  grep  shadow
[0;1mOptional Deps   :[0m tk: gitk and git gui
                  perl-libwww: git svn [installed]
                  perl-term-readkey: git svn and interactive.singlekey setting
                  perl-mime-tools: git send-email
                  perl-net-smtp-ssl: git send-email TLS support
                  perl-authen-sasl: git send-email TLS support
                  perl-mediawiki-api: git mediawiki support
                  perl-datetime-format-iso8601: git mediawiki support
                  perl-lwp-protocol-https: git mediawiki https support
                  perl-cgi: gitweb (web interface) support
                  python: git svn & git p4 [installed]
                  subversion: git svn
                  org.freedesktop.secrets: keyring credential helper
                  libsecret: libsecret credential helper [installed]
[0;1mRequired By     :[0m gitlab  gitlab-shell  yay-git  youtube-cli
[0;1mOptional For    :[0m atom  gettext
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 30.42 MiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Fri 30 Oct 2020 12:36:30 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:32:42 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gitlab
[0;1mVersion         :[0m 13.6.2-1
[0;1mDescription     :[0m Project management and code hosting application
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://gitlab.com/gitlab-org/gitlab-foss
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ruby  ruby-bundler  git  gitlab-workhorse  gitlab-gitaly  openssh  redis  libxslt  icu  re2  http-parser  nodejs  openssl  gitlab-shell
[0;1mOptional Deps   :[0m postgresql: database backend
                  python-docutils: reStructuredText markup language support
                  smtp-server: mail server in order to receive mail notifications
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 946.47 MiB
[0;1mPackager        :[0m Anatol Pomozov <anatol.pomozov@gmail.com>
[0;1mBuild Date      :[0m Tue 08 Dec 2020 08:35:30 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:14:26 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gotop-bin
[0;1mVersion         :[0m 4.0.0-1
[0;1mDescription     :[0m A terminal based graphical activity monitor inspired by gtop and vtop
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/xxxserxxx/gotop
[0;1mLicenses        :[0m AGPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m gotop
[0;1mDepends On      :[0m glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m gotop
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 8.09 MiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Wed 02 Dec 2020 01:30:49 PM MSK
[0;1mInstall Date    :[0m Wed 02 Dec 2020 01:31:08 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m gpaste
[0;1mVersion         :[0m 3.38.3-1
[0;1mDescription     :[0m Clipboard management system
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.imagination-land.org/tags/GPaste.html
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gtk3
[0;1mOptional Deps   :[0m wgetpaste: Upload clipboard contents
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1439.57 KiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Mon 02 Nov 2020 06:53:22 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 05:27:20 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m grep
[0;1mVersion         :[0m 3.6-1
[0;1mDescription     :[0m A string search utility
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/grep/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  pcre
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m base  bashtop  git  mkinitcpio  redis
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 738.89 KiB
[0;1mPackager        :[0m Sébastien Luttringer <seblu@seblu.net>
[0;1mBuild Date      :[0m Mon 09 Nov 2020 12:41:56 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 04:46:36 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m groff
[0;1mVersion         :[0m 1.22.4-3
[0;1mDescription     :[0m GNU troff text-formatting system
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/groff/groff.html
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m perl  gcc-libs
[0;1mOptional Deps   :[0m netpbm: for use together with man -H command interaction in browsers [installed]
                  psutils: for use together with man -H command interaction in browsers
                  libxaw: for gxditview [installed]
                  perl-file-homedir: for use with glilypond
[0;1mRequired By     :[0m man-db
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 9.57 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Mon 23 Dec 2019 03:21:05 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:48 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m grub
[0;1mVersion         :[0m 2:2.04-8
[0;1mDescription     :[0m GNU GRand Unified Bootloader (2)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/grub/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m grub-common  grub-bios  grub-emu  grub-efi-x86_64
[0;1mDepends On      :[0m sh  xz  gettext  device-mapper
[0;1mOptional Deps   :[0m freetype2: For grub-mkfont usage [installed]
                  fuse2: For grub-mount usage [installed]
                  dosfstools: For grub-mkrescue FAT FS and EFI support [installed]
                  efibootmgr: For grub-install EFI support [installed]
                  libisoburn: Provides xorriso for generating grub rescue iso using grub-mkrescue
                  os-prober: To detect other OSes when generating grub.cfg in BIOS systems [installed]
                  mtools: For grub-mkrescue FAT FS support [installed]
[0;1mRequired By     :[0m grub-customizer
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m grub-common  grub-bios  grub-emu  grub-efi-x86_64  grub-legacy
[0;1mReplaces        :[0m grub-common  grub-bios  grub-emu  grub-efi-x86_64
[0;1mInstalled Size  :[0m 32.91 MiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Wed 29 Jul 2020 11:51:43 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:19:57 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m grub-customizer
[0;1mVersion         :[0m 5.1.0-2
[0;1mDescription     :[0m A graphical grub2 settings manager
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://launchpad.net/grub-customizer
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m grub  gtkmm3  libarchive
[0;1mOptional Deps   :[0m hwinfo: Additional hardware information
                  polkit: Run grub-customizer from menu [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.09 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 08:06:07 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 01:49:51 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gtop
[0;1mVersion         :[0m 1.0.2-2
[0;1mDescription     :[0m System monitoring dashboard for terminal
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/aksakalli/gtop
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m nodejs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 11.48 MiB
[0;1mPackager        :[0m Sven-Hendrik Haase <svenstaro@gmail.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:01:05 AM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:22:32 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gucharmap
[0;1mVersion         :[0m 13.0.0-1
[0;1mDescription     :[0m Gnome Unicode Charmap
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://wiki.gnome.org/Apps/Gucharmap
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m libgucharmap_2_90.so=7-64
[0;1mDepends On      :[0m dconf  gtk3
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 9.02 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
[0;1mBuild Date      :[0m Fri 13 Mar 2020 06:57:39 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 07:52:28 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m gzip
[0;1mVersion         :[0m 1.10-3
[0;1mDescription     :[0m GNU compression utility
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/gzip/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  bash  less
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m base  mkinitcpio  texinfo
[0;1mOptional For    :[0m man-db
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 156.95 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Wed 13 Nov 2019 06:54:22 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:44 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m haskell-graphviz
[0;1mVersion         :[0m 2999.20.1.0-28
[0;1mDescription     :[0m Bindings to Graphviz for graph visualisation.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://hackage.haskell.org/package/graphviz
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ghc-libs  haskell-colour  haskell-dlist  haskell-fgl  haskell-polyparse  haskell-temporary  haskell-wl-pprint-text
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 16.84 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Fri 27 Nov 2020 06:32:55 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:14:49 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m haskell-skylighting
[0;1mVersion         :[0m 0.10.0.1-5
[0;1mDescription     :[0m Syntax highlighting library
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/jgm/skylighting
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ghc-libs  haskell-skylighting-core
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 8.07 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Wed 09 Dec 2020 05:22:33 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:14:49 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m htop
[0;1mVersion         :[0m 3.0.3-1
[0;1mDescription     :[0m Interactive process viewer
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://htop.dev/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ncurses  libncursesw.so=6-64  libnl
[0;1mOptional Deps   :[0m lm_sensors: show cpu temperatures [installed]
                  lsof: show files opened by a process [installed]
                  strace: attach to a running process
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 275.54 KiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Mon 07 Dec 2020 10:37:17 AM MSK
[0;1mInstall Date    :[0m Thu 10 Dec 2020 01:46:38 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m inkscape
[0;1mVersion         :[0m 1.0.1-3
[0;1mDescription     :[0m Professional vector graphics editor
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://inkscape.org/
[0;1mLicenses        :[0m GPL  LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m aspell  dbus-glib  double-conversion  gc  gdl  gsl  gtkmm3  gtkspell3  libcdr  libjpeg-turbo  libmagick6  libvisio  libxslt  poppler-glib  potrace  python  ttf-font
[0;1mOptional Deps   :[0m fig2dev: xfig input
                  gvfs: import clip art [installed]
                  pstoedit: latex formulas
                  python-lxml: some extensions
                  python-numpy: some extensions
                  scour: optimized SVG output, some extensions
                  texlive-core: latex formulas
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 154.70 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Wed 02 Dec 2020 10:44:44 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:14:54 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m intltool
[0;1mVersion         :[0m 0.51.0-6
[0;1mDescription     :[0m The internationalization tool collection
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://launchpad.net/intltool
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m perl-xml-parser
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 149.56 KiB
[0;1mPackager        :[0m Jelle van der Waa <jelle@vdwaa.nl>
[0;1mBuild Date      :[0m Sun 21 Jun 2020 06:20:59 PM MSK
[0;1mInstall Date    :[0m Tue 10 Nov 2020 11:25:45 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m iwd
[0;1mVersion         :[0m 1.9-1
[0;1mDescription     :[0m Internet Wireless Daemon
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://git.kernel.org/cgit/network/wireless/iwd.git/
[0;1mLicenses        :[0m LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  readline  libreadline.so=8-64
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m connman  networkmanager
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.57 MiB
[0;1mPackager        :[0m Christian Rebischke <Chris.Rebischke@archlinux.org>
[0;1mBuild Date      :[0m Wed 09 Sep 2020 07:30:13 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 11:08:31 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m kernel-headers-musl
[0;1mVersion         :[0m 4.19.88-1
[0;1mDescription     :[0m Linux kernel headers sanitized for use with musl libc
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/sabotage-linux/kernel-headers
[0;1mLicenses        :[0m LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m musl
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 4.55 MiB
[0;1mPackager        :[0m Eli Schwartz <eschwartz@archlinux.org>
[0;1mBuild Date      :[0m Sun 23 Feb 2020 06:44:57 PM MSK
[0;1mInstall Date    :[0m Mon 09 Nov 2020 04:11:24 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m kitty
[0;1mVersion         :[0m 0.19.2-2
[0;1mDescription     :[0m A modern, hackable, featureful, OpenGL-based terminal emulator
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/kovidgoyal/kitty
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python3  freetype2  fontconfig  wayland  libx11  libxkbcommon-x11  libxi  hicolor-icon-theme  libgl  libcanberra  dbus  lcms2  kitty-terminfo
[0;1mOptional Deps   :[0m imagemagick: viewing images with icat [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 12.86 MiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Sat 28 Nov 2020 03:45:16 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:07 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m lib32-gcc-libs
[0;1mVersion         :[0m 10.2.0-4
[0;1mDescription     :[0m 32-bit runtime libraries shipped by GCC
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://gcc.gnu.org
[0;1mLicenses        :[0m GPL  LGPL  FDL  custom
[0;1mGroups          :[0m multilib-devel
[0;1mProvides        :[0m libgo.so=16-32  libgfortran.so=5-32  libubsan.so=1-32  libasan.so=6-32
[0;1mDepends On      :[0m lib32-glibc>=2.27
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m steamcmd
[0;1mOptional For    :[0m gcc
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 104.67 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Mon 09 Nov 2020 02:43:35 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:10 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m libclc
[0;1mVersion         :[0m 11.0.0-1
[0;1mDescription     :[0m Library requirements of the OpenCL C programming language
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://libclc.llvm.org/
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 71.07 MiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Sat 17 Oct 2020 04:16:33 AM MSK
[0;1mInstall Date    :[0m Tue 08 Dec 2020 01:15:59 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m libkvkontakte
[0;1mVersion         :[0m 5.0.0-3
[0;1mDescription     :[0m C++ library for asynchronous interaction with VK social network via its web API
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.digikam.org
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m kdewebkit
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 344.50 KiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Fri 08 May 2020 12:33:07 PM MSK
[0;1mInstall Date    :[0m Sun 06 Dec 2020 03:57:45 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m libtool
[0;1mVersion         :[0m 2.4.6+42+gb88cebd5-14
[0;1mDescription     :[0m A generic library support script
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/libtool
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m libltdl=2.4.6+42+gb88cebd5  libtool-multilib=2.4.6+42+gb88cebd5
[0;1mDepends On      :[0m sh  tar  glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m docker  fontforge  guile  hwloc  imagemagick  libcanberra  libmagick6  moc  mpg123  openmpi  pulseaudio
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m libltdl  libtool-multilib
[0;1mReplaces        :[0m libltdl  libtool-multilib
[0;1mInstalled Size  :[0m 2.19 MiB
[0;1mPackager        :[0m Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
[0;1mBuild Date      :[0m Sun 09 Aug 2020 06:24:16 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:48 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m libunrar
[0;1mVersion         :[0m 1:6.0.3-1
[0;1mDescription     :[0m Library and header file for applications that use libunrar
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.rarlab.com/rar_add.htm
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gcc-libs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 309.51 KiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Mon 07 Dec 2020 02:38:20 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:13 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m libva-vdpau-driver
[0;1mVersion         :[0m 0.7.4-5
[0;1mDescription     :[0m VDPAU backend for VA API
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://freedesktop.org/wiki/Software/vaapi
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libgl  libvdpau  libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m libva  vlc
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 101.78 KiB
[0;1mPackager        :[0m Maxime Gauduin <alucryd@archlinux.org>
[0;1mBuild Date      :[0m Sun 06 Dec 2020 12:53:32 PM MSK
[0;1mInstall Date    :[0m Tue 08 Dec 2020 01:15:22 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m linux
[0;1mVersion         :[0m 5.9.13.arch1-1
[0;1mDescription     :[0m The Linux kernel and modules
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://git.archlinux.org/linux.git/log/?h=v5.9.13-arch1
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m VIRTUALBOX-GUEST-MODULES  WIREGUARD-MODULE
[0;1mDepends On      :[0m coreutils  kmod  initramfs
[0;1mOptional Deps   :[0m crda: to set the correct wireless channels of your country
                  linux-firmware: firmware images needed for some devices [installed]
[0;1mRequired By     :[0m nvidia
[0;1mOptional For    :[0m base
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m virtualbox-guest-modules-arch  wireguard-arch
[0;1mInstalled Size  :[0m 79.07 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Tue 08 Dec 2020 03:09:55 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:26 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m linux-firmware
[0;1mVersion         :[0m 20201120.bc9cd0b-1
[0;1mDescription     :[0m Firmware files for Linux
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=summary
[0;1mLicenses        :[0m GPL2  GPL3  custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m linux
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 605.75 MiB
[0;1mPackager        :[0m Laurent Carlier <lordheavym@gmail.com>
[0;1mBuild Date      :[0m Fri 27 Nov 2020 08:48:23 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:23:58 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m linux-headers
[0;1mVersion         :[0m 5.9.13.arch1-1
[0;1mDescription     :[0m Headers and scripts for building modules for the Linux kernel
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://git.archlinux.org/linux.git/log/?h=v5.9.13-arch1
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 123.31 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Tue 08 Dec 2020 03:09:55 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:35 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m lynx
[0;1mVersion         :[0m 2.8.9-3
[0;1mDescription     :[0m A text browser for the World Wide Web
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://lynx.browser.org/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m openssl  libidn
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m ranger
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 5.00 MiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 12:09:19 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:02:30 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m m4
[0;1mVersion         :[0m 1.4.18-3
[0;1mDescription     :[0m The GNU macro processor
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/m4
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  bash
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m autoconf  bison  flex
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 260.55 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Wed 13 Nov 2019 08:50:25 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m make
[0;1mVersion         :[0m 4.3-3
[0;1mDescription     :[0m GNU make utility to maintain groups of programs
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/make
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  guile
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1551.73 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Wed 08 Apr 2020 04:37:19 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:49 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m moc
[0;1mVersion         :[0m 1:2.5.2-3
[0;1mDescription     :[0m An ncurses console audio player designed to be powerful and easy to use
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://moc.daper.net/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libmad  libid3tag  jack  curl  libltdl  file
[0;1mOptional Deps   :[0m speex: for using the speex plugin [installed]
                  ffmpeg: for using the ffmpeg plugin [installed]
                  taglib: for using the musepack plugin [installed]
                  libmpcdec: for using the musepack plugin [installed]
                  wavpack: for using the wavpack plugin [installed]
                  faad2: for using the aac plugin [installed]
                  libmodplug: for using the modplug plugin [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 592.91 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sun 08 Mar 2020 12:45:14 PM MSK
[0;1mInstall Date    :[0m Fri 20 Nov 2020 01:14:01 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m monitoring-plugins
[0;1mVersion         :[0m 2.2-6
[0;1mDescription     :[0m Plugins for Icinga, Naemon, Nagios, Shinken, Sensu and other monitoring applications
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.monitoring-plugins.org
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m nagios-plugins
[0;1mDepends On      :[0m perl
[0;1mOptional Deps   :[0m net-snmp: for SNMP checking
                  postgresql-libs: for check_pgsql
                  mariadb-libs: for check_mysql_query and check_mysql
                  libdbi: for check_dbi
                  libldap: for check_ldap [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m nagios-plugins
[0;1mReplaces        :[0m nagios-plugins
[0;1mInstalled Size  :[0m 2.68 MiB
[0;1mPackager        :[0m Jonathan Steel <jsteel@archlinux.org>
[0;1mBuild Date      :[0m Tue 21 Apr 2020 08:04:44 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:28:14 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m mpc
[0;1mVersion         :[0m 0.33-3
[0;1mDescription     :[0m Minimalist command line interface to MPD
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.musicpd.org/clients/mpc/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  libmpdclient.so=2-64
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 117.12 KiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Thu 03 Sep 2020 08:52:51 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 02:38:05 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m mpd
[0;1mVersion         :[0m 0.22.3-1
[0;1mDescription     :[0m Flexible, powerful, server-side application for playing music
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.musicpd.org/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bzip2  chromaprint  gcc-libs  glibc  lame  libcdio  libcdio-paranoia  libgcrypt  libgme  libmad  libmms  libmodplug  libmpcdec  libnfs  libshout  libsidplayfp  libsoxr  openal  opus  smbclient  sqlite  wavpack  wildmidi  yajl  zlib  zziplib  libFLAC.so=8-64  libao.so=4-64  libasound.so=2-64  libaudiofile.so=1-64  libavahi-client.so=3-64  libavahi-common.so=3-64  libavcodec.so=58-64  libavformat.so=58-64  libavutil.so=56-64  libcurl.so=4-64  libdbus-1.so=3-64  libexpat.so=1-64  libfaad.so=2-64  libfluidsynth.so=2-64  libicui18n.so=67-64  libicuuc.so=67-64  libid3tag.so=0-64  libjack.so=0-64  libmikmod.so=3-64  libmpdclient.so=2-64  libmpg123.so=0-64  libogg.so=0-64  libpulse.so=0-64  libsamplerate.so=0-64  libsndfile.so=1-64  libsystemd.so=0-64  libtwolame.so=0-64  libupnp.so=17-64  liburing.so=1-64  libvorbis.so=0-64  libvorbisenc.so=2-64
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.65 MiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Fri 06 Nov 2020 07:32:11 PM MSK
[0;1mInstall Date    :[0m Mon 07 Dec 2020 08:28:06 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m mtools
[0;1mVersion         :[0m 4.0.26-1
[0;1mDescription     :[0m A collection of utilities to access MS-DOS disks
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/mtools/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bash  glibc
[0;1mOptional Deps   :[0m libx11: required by floppyd [installed]
                  libxau: required by floppyd [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m grub
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 388.45 KiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Sat 28 Nov 2020 04:46:37 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:12 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m nautilus
[0;1mVersion         :[0m 3.38.2-1
[0;1mDescription     :[0m Default file manager for GNOME
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://wiki.gnome.org/Apps/Files
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m gnome
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libgexiv2  gnome-desktop  gvfs  dconf  tracker3  tracker3-miners  gnome-autoar  gst-plugins-base-libs  libnautilus-extension
[0;1mOptional Deps   :[0m nautilus-sendto: Send files via mail extension
[0;1mRequired By     :[0m gnome-panel
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 13.51 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Fri 20 Nov 2020 05:26:55 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:13 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ncmpcpp
[0;1mVersion         :[0m 0.8.2-13
[0;1mDescription     :[0m Almost exact clone of ncmpc with some new features
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://ncmpcpp.rybczak.net/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m curl  libmpdclient  taglib  ncurses  fftw  boost-libs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.73 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Mon 07 Dec 2020 12:59:04 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:35 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m neofetch
[0;1mVersion         :[0m 7.1.0-2
[0;1mDescription     :[0m A CLI system information tool written in BASH that supports displaying images.
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/dylanaraps/neofetch
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bash
[0;1mOptional Deps   :[0m catimg: Display Images
                  chafa: Image to text support
                  feh: Wallpaper Display [installed]
                  imagemagick: Image cropping / Thumbnail creation / Take a screenshot [installed]
                  jp2a: Display Images
                  libcaca: Display Images [installed]
                  nitrogen: Wallpaper Display [installed]
                  w3m: Display Images [installed]
                  xdotool: See https://github.com/dylanaraps/neofetch/wiki/Images-in-the-terminal [installed]
                  xorg-xdpyinfo: Resolution detection (Single Monitor) [installed]
                  xorg-xprop: Desktop Environment and Window Manager [installed]
                  xorg-xrandr: Resolution detection (Multi Monitor + Refresh rates) [installed]
                  xorg-xwininfo: See https://github.com/dylanaraps/neofetch/wiki/Images-in-the-terminal [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 339.13 KiB
[0;1mPackager        :[0m Morten Linderud <foxboron@archlinux.org>
[0;1mBuild Date      :[0m Fri 02 Oct 2020 06:07:26 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:19:36 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m neomutt
[0;1mVersion         :[0m 20201127-1
[0;1mDescription     :[0m A version of mutt with added features
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://neomutt.org/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  gpgme  lua53  notmuch-runtime  krb5  gnutls  sqlite  libsasl  ncurses  libidn2  lmdb  gdbm  kyotocabinet  lz4  zlib  zstd  db
[0;1mOptional Deps   :[0m python: keybase.py [installed]
                  perl: smime_keys [installed]
                  ca-certificates: default CA certificates [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 8.49 MiB
[0;1mPackager        :[0m Frederik Schwan <freswa@archlinux.org>
[0;1mBuild Date      :[0m Fri 27 Nov 2020 07:09:16 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:15 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m neovim
[0;1mVersion         :[0m 0.4.4-1
[0;1mDescription     :[0m Fork of Vim aiming to improve user experience, plugins, and GUIs
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://neovim.io
[0;1mLicenses        :[0m custom:neovim
[0;1mGroups          :[0m None
[0;1mProvides        :[0m vim-plugin-runtime
[0;1mDepends On      :[0m libtermkey  libuv  msgpack-c  unibilium  libvterm  luajit  libluv
[0;1mOptional Deps   :[0m python-neovim: for Python 3 plugin support (see :help python)
                  xclip: for clipboard support on X11 (or xsel) (see :help clipboard) [installed]
                  xsel: for clipboard support on X11 (or xclip) (see :help clipboard)
                  wl-clipboard: for clipboard support on wayland (see :help clipboard)
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 20.45 MiB
[0;1mPackager        :[0m Sven-Hendrik Haase <svenstaro@gmail.com>
[0;1mBuild Date      :[0m Wed 05 Aug 2020 11:16:43 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:25:04 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m nerd-fonts-mononoki
[0;1mVersion         :[0m 2.1.0-1
[0;1mDescription     :[0m Patched font Mononoki from nerd-fonts library
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/ryanoasis/nerd-fonts
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 6.63 MiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sat 07 Nov 2020 01:20:14 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:20:15 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m None

[0;1mName            :[0m net-tools
[0;1mVersion         :[0m 1.60.20181103git-2
[0;1mDescription     :[0m Configuration tools for Linux networking
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://net-tools.sourceforge.net/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m aircrack-ng
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 545.22 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Wed 13 Nov 2019 08:21:34 PM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:17:31 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m netctl
[0;1mVersion         :[0m 1.24-1
[0;1mDescription     :[0m Profile based systemd network management
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://projects.archlinux.org/netctl.git/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m coreutils  iproute2  resolvconf  systemd>=233
[0;1mOptional Deps   :[0m dialog: for the menu based wifi assistant [installed]
                  dhclient: for DHCP support (or dhcpcd)
                  dhcpcd: for DHCP support (or dhclient)
                  wpa_supplicant: for wireless networking support [installed]
                  ifplugd: for automatic wired connections through netctl-ifplugd
                  ppp: for PPP connections [installed]
                  openvswitch: for Open vSwitch connections
                  wireguard-tools: for WireGuard connections
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 95.68 KiB
[0;1mPackager        :[0m Florian Pritz <bluewind@xinu.at>
[0;1mBuild Date      :[0m Sat 31 Oct 2020 04:28:25 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:35 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m netdata
[0;1mVersion         :[0m 1.26.0-1
[0;1mDescription     :[0m Real-time performance monitoring, in the greatest possible detail, over the web
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/netdata/netdata
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libmnl  libnetfilter_acct  zlib  judy  libuv  json-c  libcap  lz4  openssl  which  snappy  protobuf
[0;1mOptional Deps   :[0m nodejs: for monitoring named and SNMP devices [installed]
                  lm_sensors: for monitoring hardware sensors [installed]
                  iproute2: for monitoring Linux QoS [installed]
                  python: for most of the external plugins [installed]
                  python-psycopg2: for monitoring PostgreSQL databases
                  python-mysqlclient: for monitoring MySQL/MariaDB databases
                  python-requests: for monitoring elasticsearch [installed]
                  hddtemp: for monitoring hhd temperature
                  apcupsd: for monitoring APC UPS [installed]
                  iw: for monitoring Linux as access point [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 8.18 MiB
[0;1mPackager        :[0m Sven-Hendrik Haase <svenstaro@gmail.com>
[0;1mBuild Date      :[0m Thu 15 Oct 2020 03:50:30 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:28:31 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m netpbm
[0;1mVersion         :[0m 10.73.33-1
[0;1mDescription     :[0m A toolkit for manipulation of graphic images
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://netpbm.sourceforge.net/
[0;1mLicenses        :[0m custom  BSD  GPL  LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m perl  libpng  libtiff  libxml2
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m groff
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 7.44 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Sun 27 Sep 2020 08:50:36 PM MSK
[0;1mInstall Date    :[0m Wed 25 Nov 2020 07:52:23 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m network-manager-applet
[0;1mVersion         :[0m 1.18.0-1
[0;1mDescription     :[0m Applet for managing network connections
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://wiki.gnome.org/Projects/NetworkManager/
[0;1mLicenses        :[0m GPL  LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m nm-connection-editor  libmm-glib  libnotify  libsecret  networkmanager  libappindicator-gtk3
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 658.49 KiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Mon 22 Jun 2020 11:33:31 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 06:43:39 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m networkmanager
[0;1mVersion         :[0m 1.26.4-1
[0;1mDescription     :[0m Network connection manager and user applications
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://wiki.gnome.org/Projects/NetworkManager
[0;1mLicenses        :[0m GPL2  LGPL2.1
[0;1mGroups          :[0m gnome
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libnm  iproute2  polkit  wpa_supplicant  libmm-glib  libnewt  libndp  libteam  curl  bluez-libs  libpsl  audit  mobile-broadband-provider-info
[0;1mOptional Deps   :[0m dnsmasq: connection sharing
                  bluez: Bluetooth support
                  ppp: dialup connection support [installed]
                  modemmanager: cellular network support
                  iwd: wpa_supplicant alternative [installed]
                  dhclient: alternative DHCP client
                  openresolv: alternative resolv.conf manager [installed]
                  firewalld: Firewall support
[0;1mRequired By     :[0m network-manager-applet  networkmanager-qt
[0;1mOptional For    :[0m firefox  libproxy
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 16.09 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Wed 14 Oct 2020 05:10:13 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:25:04 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m networkmanager-openconnect
[0;1mVersion         :[0m 1.2.7dev+49+gc512d5a-1
[0;1mDescription     :[0m NetworkManager VPN plugin for OpenConnect
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://wiki.gnome.org/Projects/NetworkManager
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libnm  libsecret  openconnect  libopenconnect.so=5-64
[0;1mOptional Deps   :[0m libnma: GUI support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.74 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Sun 18 Oct 2020 02:39:39 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 06:43:39 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m networkmanager-openvpn
[0;1mVersion         :[0m 1.8.12-1
[0;1mDescription     :[0m NetworkManager VPN plugin for OpenVPN
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://wiki.gnome.org/Projects/NetworkManager
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libnm  libsecret  openvpn
[0;1mOptional Deps   :[0m libnma: GUI support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1292.71 KiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
[0;1mBuild Date      :[0m Sat 07 Mar 2020 12:24:49 AM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 10:21:25 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m networkmanager-pptp
[0;1mVersion         :[0m 1.2.9dev+10+gb41b0d0-2
[0;1mDescription     :[0m NetworkManager VPN plugin for PPTP
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://wiki.gnome.org/Projects/NetworkManager
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libnm  libsecret  ppp=2.4.7  pptpclient
[0;1mOptional Deps   :[0m libnma: GUI support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 662.38 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 19 May 2020 11:34:47 AM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 10:21:25 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m networkmanager-qt
[0;1mVersion         :[0m 5.76.0-1
[0;1mDescription     :[0m Qt wrapper for NetworkManager API
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://community.kde.org/Frameworks
[0;1mLicenses        :[0m LGPL
[0;1mGroups          :[0m kf5
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m networkmanager  qt5-base
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.92 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Sun 08 Nov 2020 02:39:17 AM MSK
[0;1mInstall Date    :[0m Thu 19 Nov 2020 10:54:37 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m nitrogen
[0;1mVersion         :[0m 1.6.1-3
[0;1mDescription     :[0m Background browser and setter for X windows
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://projects.l3ib.org/nitrogen/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gtkmm  hicolor-icon-theme  librsvg
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m neofetch
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 459.01 KiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Thu 16 Apr 2020 10:52:32 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:04:36 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m nmap
[0;1mVersion         :[0m 7.91-1
[0;1mDescription     :[0m Utility for network discovery and security auditing
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://nmap.org/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  pcre  openssl  lua53  libpcap  libssh2  libssh2.so=1-64  zlib  gcc-libs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 24.00 MiB
[0;1mPackager        :[0m Levente Polyak <anthraxx@archlinux.org>
[0;1mBuild Date      :[0m Wed 11 Nov 2020 01:07:21 AM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 10:23:15 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m nnn
[0;1mVersion         :[0m 3.5-1
[0;1mDescription     :[0m The fastest terminal file manager ever written.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/jarun/nnn
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bash
[0;1mOptional Deps   :[0m atool: for more archive formats
                  libarchive: for more archive formats [installed]
                  zip: for zip archive format
                  unzip: for zip archive format [installed]
                  trash-cli: to trash files
                  sshfs: mount remotes
                  rclone: mount remotes
                  fuse2: unmount remotes [installed]
                  xdg-utils: desktop opener [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 260.67 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Fri 20 Nov 2020 08:39:14 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:16 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m nvidia
[0;1mVersion         :[0m 455.45.01-6
[0;1mDescription     :[0m NVIDIA drivers for linux
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.nvidia.com/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m NVIDIA-MODULE
[0;1mDepends On      :[0m linux  nvidia-utils=455.45.01  libglvnd
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 18.19 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Tue 08 Dec 2020 04:01:19 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:36 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m nvidia-settings
[0;1mVersion         :[0m 455.45.01-1
[0;1mDescription     :[0m Tool for configuring the NVIDIA graphics driver
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/NVIDIA/nvidia-settings
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m jansson  gtk3  libxv  libvdpau  nvidia-utils  libxnvctrl
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m nvidia-utils
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1607.75 KiB
[0;1mPackager        :[0m Sven-Hendrik Haase <svenstaro@gmail.com>
[0;1mBuild Date      :[0m Sat 21 Nov 2020 10:25:52 PM MSK
[0;1mInstall Date    :[0m Tue 08 Dec 2020 12:38:09 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m nvtop
[0;1mVersion         :[0m 1.1.0-1
[0;1mDescription     :[0m An htop like monitoring tool for NVIDIA GPUs
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/Syllo/nvtop
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m nvidia-utils  ncurses
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 59.18 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 05 Dec 2020 12:56:20 AM MSK
[0;1mInstall Date    :[0m Tue 08 Dec 2020 12:35:32 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m obs-studio
[0;1mVersion         :[0m 26.0.2-3
[0;1mDescription     :[0m Free, open source software for live streaming and recording
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://obsproject.com
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ffmpeg  jansson  libxinerama  libxkbcommon-x11  mbedtls  qt5-svg  qt5-x11extras  curl  jack  gtk-update-icon-cache
[0;1mOptional Deps   :[0m libfdk-aac: FDK AAC codec support
                  libxcomposite: XComposite capture support [installed]
                  libva-intel-driver: hardware encoding
                  libva-mesa-driver: hardware encoding
                  luajit: scripting support [installed]
                  python: scripting support [installed]
                  vlc: VLC Media Source support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 14.47 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Thu 12 Nov 2020 09:08:58 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:37 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m os-prober
[0;1mVersion         :[0m 1.77-2
[0;1mDescription     :[0m Utility to detect other OSes on a set of drives
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://joeyh.name/code/os-prober/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m grub
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 56.97 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 08:50:22 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:20:38 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m osdbattery
[0;1mVersion         :[0m 1.4-8
[0;1mDescription     :[0m Displays battery information in the OSD style
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://osdbattery.sourceforge.net/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xosd
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 15.11 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 07:06:34 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:43:35 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pacman
[0;1mVersion         :[0m 5.2.2-1
[0;1mDescription     :[0m A library-based package manager with dependency support
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.archlinux.org/pacman/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m libalpm.so=12-64
[0;1mDepends On      :[0m bash  glibc  libarchive  curl  gpgme  pacman-mirrorlist  archlinux-keyring
[0;1mOptional Deps   :[0m perl-locale-gettext: translation support in makepkg-template
[0;1mRequired By     :[0m appstream-glib  base  yay-git
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 4.43 MiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Wed 01 Jul 2020 04:52:38 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:44 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pamixer
[0;1mVersion         :[0m 1.4-4
[0;1mDescription     :[0m Pulseaudio command-line mixer like amixer
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/cdemoulins/pamixer
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libpulse  boost-libs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 109.95 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Mon 07 Dec 2020 01:02:30 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m patch
[0;1mVersion         :[0m 2.7.6-8
[0;1mDescription     :[0m A utility to apply patch files to original sources
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/patch/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  attr
[0;1mOptional Deps   :[0m ed: for patch -e functionality
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 197.75 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Wed 13 Nov 2019 03:05:02 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:49 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pavucontrol
[0;1mVersion         :[0m 1:4.0-2
[0;1mDescription     :[0m PulseAudio Volume Control
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://freedesktop.org/software/pulseaudio/pavucontrol/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libcanberra-pulse  gtkmm3  libsigc++
[0;1mOptional Deps   :[0m pulseaudio: Audio backend [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 931.42 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Tue 19 May 2020 11:59:22 AM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 09:41:50 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pcmanfm
[0;1mVersion         :[0m 1.3.1-2
[0;1mDescription     :[0m Extremely fast and lightweight file manager
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://lxde.org/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m lxde
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libfm-gtk2  lxmenu-data
[0;1mOptional Deps   :[0m gvfs: for trash support, mounting with udisks and remote filesystems [installed]
                  xarchiver: archive management
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m xdg-utils
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1391.08 KiB
[0;1mPackager        :[0m Balló György <ballogyor+arch@gmail.com>
[0;1mBuild Date      :[0m Thu 28 May 2020 12:23:00 PM MSK
[0;1mInstall Date    :[0m Thu 19 Nov 2020 01:47:22 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m picom
[0;1mVersion         :[0m 8.2-1
[0;1mDescription     :[0m X compositor that may fix tearing issues
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/yshui/picom
[0;1mLicenses        :[0m MIT  MPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m compton
[0;1mDepends On      :[0m hicolor-icon-theme  libconfig  libdbus  libev  libgl  pcre  pixman  xcb-util-image  xcb-util-renderutil
[0;1mOptional Deps   :[0m dbus: For controlling picom via D-Bus [installed]
                  python: For running picom-convgen.py [installed]
                  xorg-xwininfo: For picom-trans [installed]
                  xorg-xprop: For picom-trans [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m compton
[0;1mReplaces        :[0m compton
[0;1mInstalled Size  :[0m 375.46 KiB
[0;1mPackager        :[0m Alexander Rødseth <rodseth@gmail.com>
[0;1mBuild Date      :[0m Mon 26 Oct 2020 05:39:38 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:04:36 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pkgconf
[0;1mVersion         :[0m 1.7.3-1
[0;1mDescription     :[0m Package compiler and linker metadata toolkit
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://git.sr.ht/~kaniini/pkgconf
[0;1mLicenses        :[0m custom:ISC
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m pkg-config  pkgconfig
[0;1mDepends On      :[0m glibc  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m pkg-config
[0;1mReplaces        :[0m pkg-config
[0;1mInstalled Size  :[0m 156.33 KiB
[0;1mPackager        :[0m Johannes Löthberg <demize@archlinux.org>
[0;1mBuild Date      :[0m Sun 31 May 2020 07:47:03 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:49 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m playerctl
[0;1mVersion         :[0m 2.3.1-1
[0;1mDescription     :[0m mpris media player controller and lib for spotify, vlc, audacious, bmp, xmms2, and others.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/acrisci/playerctl
[0;1mLicenses        :[0m LGPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glib2
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 447.57 KiB
[0;1mPackager        :[0m Maxim Baz <pgp@maximbaz.com>
[0;1mBuild Date      :[0m Wed 02 Dec 2020 12:54:05 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:41 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pulseaudio
[0;1mVersion         :[0m 14.0-1
[0;1mDescription     :[0m A featureful, general-purpose sound server
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.freedesktop.org/wiki/Software/PulseAudio/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libpulse=14.0-1  alsa-card-profiles  rtkit  libltdl  speexdsp  tdb  orc  libsoxr  webrtc-audio-processing  libxtst
[0;1mOptional Deps   :[0m pulseaudio-alsa: ALSA configuration (recommended) [installed]
                  pulseaudio-zeroconf: Zeroconf support [installed]
                  pulseaudio-lirc: IR (lirc) support
                  pulseaudio-jack: Jack support [installed]
                  pulseaudio-bluetooth: Bluetooth support
                  pulseaudio-equalizer: Graphical equalizer [installed]
                  pulseaudio-rtp: RTP and RAOP support
[0;1mRequired By     :[0m pulseaudio-alsa  pulseaudio-equalizer  pulseaudio-jack  pulseaudio-zeroconf  pulsemixer
[0;1mOptional For    :[0m deadbeef  firefox  pavucontrol
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m pulseaudio-xen<=9.0  pulseaudio-gconf<=11.1
[0;1mInstalled Size  :[0m 6.06 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Tue 24 Nov 2020 04:41:38 AM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:16 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pulseaudio-alsa
[0;1mVersion         :[0m 1:1.2.2-2
[0;1mDescription     :[0m ALSA Configuration for PulseAudio
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.alsa-project.org
[0;1mLicenses        :[0m LGPL2.1
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m alsa-plugins>=1.2.2-2  pulseaudio
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m pulseaudio
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 0.00 B
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
[0;1mBuild Date      :[0m Wed 13 May 2020 01:16:12 PM MSK
[0;1mInstall Date    :[0m Mon 09 Nov 2020 03:50:09 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pulseaudio-equalizer
[0;1mVersion         :[0m 14.0-1
[0;1mDescription     :[0m Graphical equalizer for PulseAudio
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.freedesktop.org/wiki/Software/PulseAudio/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m pulseaudio=14.0-1  python-pyqt5  python-dbus  fftw
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m pulseaudio
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 95.20 KiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Tue 24 Nov 2020 04:41:38 AM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:17 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pulseaudio-jack
[0;1mVersion         :[0m 14.0-1
[0;1mDescription     :[0m Jack support for PulseAudio
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.freedesktop.org/wiki/Software/PulseAudio/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m pulseaudio=14.0-1  jack
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m pulseaudio
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 69.04 KiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Tue 24 Nov 2020 04:41:38 AM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:17 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pulseaudio-zeroconf
[0;1mVersion         :[0m 14.0-1
[0;1mDescription     :[0m Zeroconf support for PulseAudio
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.freedesktop.org/wiki/Software/PulseAudio/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m pulseaudio=14.0-1  avahi  openssl
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m pulseaudio
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 90.48 KiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Tue 24 Nov 2020 04:41:38 AM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:17 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pulsemixer
[0;1mVersion         :[0m 1.5.1-2
[0;1mDescription     :[0m CLI and curses mixer for pulseaudio
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/GeorgeFilipkin/pulsemixer
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python  pulseaudio
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 101.71 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Thu 12 Nov 2020 08:06:23 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:41 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m pulseview
[0;1mVersion         :[0m 0.4.2-3
[0;1mDescription     :[0m A Qt based logic analyzer GUI for sigrok
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://sigrok.org/wiki/Main_Page
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libsigrok  libsigrokdecode  qt5-base  qt5-svg  boost-libs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.54 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Mon 07 Dec 2020 01:24:34 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:41 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m python-pip
[0;1mVersion         :[0m 20.2.1-1
[0;1mDescription     :[0m The PyPA recommended tool for installing Python packages
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://pip.pypa.io/
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python-appdirs  python-cachecontrol  python-colorama  python-contextlib2  python-distlib  python-distro  python-html5lib  python-packaging  python-pep517  python-progress  python-requests  python-retrying  python-resolvelib  python-setuptools  python-six  python-toml  python-pyopenssl
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m python
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1587.91 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 05 Dec 2020 01:39:32 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:47 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m qt5-networkauth
[0;1mVersion         :[0m 5.15.2-1
[0;1mDescription     :[0m Network authentication module
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.qt.io
[0;1mLicenses        :[0m GPL3  LGPL3  FDL  custom
[0;1mGroups          :[0m qt  qt5
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m qt5-base
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m python-pyqt5
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 254.39 KiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Thu 19 Nov 2020 06:43:16 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:27 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m qutebrowser
[0;1mVersion         :[0m 1.14.1-1
[0;1mDescription     :[0m A keyboard-driven, vim-like browser based on PyQt5
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.qutebrowser.org/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python-attrs  python-jinja  python-pygments  python-pypeg2  python-pyqt5  python-yaml  qt5-base  python-pyqtwebengine
[0;1mOptional Deps   :[0m gst-libav: media playback with qt5-webkit backend
                  gst-plugins-base: media playback with qt5-webkit backend [installed]
                  gst-plugins-good: media playback with qt5-webkit backend
                  gst-plugins-bad: media playback with qt5-webkit backend
                  gst-plugins-ugly: media playback with qt5-webkit backend
                  pdfjs: displaying PDF in-browser
                  qt5-webkit: alternative backend [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 7.71 MiB
[0;1mPackager        :[0m Morten Linderud <foxboron@archlinux.org>
[0;1mBuild Date      :[0m Fri 04 Dec 2020 10:33:39 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:49 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ranger
[0;1mVersion         :[0m 1.9.3-3
[0;1mDescription     :[0m Simple, vim-like file manager
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://ranger.github.io
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python
[0;1mOptional Deps   :[0m atool: for previews of archives
                  elinks: for previews of html pages
                  ffmpegthumbnailer: for video previews
                  highlight: for syntax highlighting of code
                  libcaca: for ASCII-art image previews [installed]
                  lynx: for previews of html pages [installed]
                  mediainfo: for viewing information about media files
                  odt2txt: for OpenDocument texts
                  perl-image-exiftool: for viewing information about media files [installed]
                  poppler: for pdf previews [installed]
                  python-chardet: in case of encoding detection problems [installed]
                  sudo: to use the "run as root"-feature [installed]
                  transmission-cli: for viewing bittorrent information [installed]
                  ueberzug: for previews of images [installed]
                  w3m: for previews of images and html pages [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1863.81 KiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Thu 12 Nov 2020 09:54:07 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:49 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m rofi
[0;1mVersion         :[0m 1.6.1-1
[0;1mDescription     :[0m A window switcher, application launcher and dmenu replacement
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/DaveDavenport/rofi
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxdg-basedir  startup-notification  libxkbcommon-x11  xcb-util-wm  xcb-util-xrm  librsvg
[0;1mOptional Deps   :[0m i3-wm: use as a window switcher
[0;1mRequired By     :[0m rofimoji
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 573.70 KiB
[0;1mPackager        :[0m Morten Linderud <foxboron@archlinux.org>
[0;1mBuild Date      :[0m Mon 23 Nov 2020 10:45:22 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:27 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m rofimoji
[0;1mVersion         :[0m 4.3.0-3
[0;1mDescription     :[0m Character picker for rofi
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/fdw/rofimoji
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m emoji-font  python-configargparse  python-xdg  rofi
[0;1mOptional Deps   :[0m wl-copy: for the Wayland clipboarder
                  wtype: for the Wayland typer
                  xclip: for one of the X.Org clipboarders [installed]
                  xsel: for one of the X.Org clipboarders
                  xdotool: for the X.Org typer [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1289.84 KiB
[0;1mPackager        :[0m Daniel M. Capella <polyzen@archlinux.org>
[0;1mBuild Date      :[0m Sun 06 Dec 2020 09:44:46 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:15:49 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m rxvt-unicode
[0;1mVersion         :[0m 9.22-10
[0;1mDescription     :[0m Unicode enabled rxvt-clone terminal emulator (urxvt)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://software.schmorp.de/pkg/rxvt-unicode.html
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m rxvt-unicode-terminfo  libxft  perl  startup-notification  libnsl
[0;1mOptional Deps   :[0m gtk2-perl: to use the urxvt-tabbed
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.07 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sun 21 Jun 2020 07:03:41 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:01:18 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m scrot
[0;1mVersion         :[0m 1.4-1
[0;1mDescription     :[0m Simple command-line screenshot utility for X
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/resurrecting-open-source-projects/scrot
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m giblib  libxfixes  libxcomposite
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 57.00 KiB
[0;1mPackager        :[0m Levente Polyak <anthraxx@archlinux.org>
[0;1mBuild Date      :[0m Thu 23 Jul 2020 01:06:16 AM MSK
[0;1mInstall Date    :[0m Wed 25 Nov 2020 05:36:46 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m sed
[0;1mVersion         :[0m 4.8-1
[0;1mDescription     :[0m GNU stream editor
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/sed/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  acl  attr
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m base  bashtop  fakeroot
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 735.80 KiB
[0;1mPackager        :[0m Sébastien Luttringer <seblu@seblu.net>
[0;1mBuild Date      :[0m Wed 15 Jan 2020 04:07:59 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:43 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m sensors-applet
[0;1mVersion         :[0m 3.0.0+13+g0728426-4
[0;1mDescription     :[0m Applet for GNOME Panel to display readings from hardware sensors, including CPU temperature, fan speeds and voltage readings
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://sensors-applet.sourceforge.net/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gnome-panel  libatasmart  libxnvctrl  lm_sensors
[0;1mOptional Deps   :[0m hddtemp: get HDD temperatures
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 378.09 KiB
[0;1mPackager        :[0m Balló György <ballogyor+arch@gmail.com>
[0;1mBuild Date      :[0m Thu 22 Oct 2020 03:35:10 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:29:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m shell-color-scripts
[0;1mVersion         :[0m 0.1-1
[0;1mDescription     :[0m A collection of terminal color scripts. A random color script is selected when you open bash or zsh.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://gitlab.com/dwt1/shell-color-scripts.git
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m shell-color-scripts
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m bash [installed]
                  zsh [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 144.65 KiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Tue 10 Nov 2020 07:24:18 PM MSK
[0;1mInstall Date    :[0m Mon 07 Dec 2020 03:32:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m signal-desktop
[0;1mVersion         :[0m 1.39.2-1
[0;1mDescription     :[0m Signal Private Messenger for Linux
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://signal.org
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libvips  libxss  hicolor-icon-theme
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m signal-desktop-bin
[0;1mInstalled Size  :[0m 318.36 MiB
[0;1mPackager        :[0m kpcyrd <kpcyrd@archlinux.org>
[0;1mBuild Date      :[0m Fri 11 Dec 2020 12:13:19 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:01 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m source-highlight
[0;1mVersion         :[0m 3.1.9-3
[0;1mDescription     :[0m Convert source code to syntax highlighted document
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/src-highlite/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bash  boost-libs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.15 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sun 06 Dec 2020 10:03:01 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:01 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m spotifyd
[0;1mVersion         :[0m 0.2.24-2
[0;1mDescription     :[0m Leightweigt spotify streaming daemon with spotify connect support
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/Spotifyd/spotifyd
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m alsa-lib  libogg  libpulse  dbus
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 11.56 MiB
[0;1mPackager        :[0m Frederik Schwan <freswa@archlinux.org>
[0;1mBuild Date      :[0m Tue 04 Aug 2020 04:10:31 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 02:38:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m st-distrotube-git
[0;1mVersion         :[0m 0.8.2.r3.30cc3b0-1
[0;1mDescription     :[0m A heavily-patched and customized build of st (the Suckless simple terminal) from DistroTube.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://gitlab.com/dwt1/st-distrotube.git
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m st
[0;1mDepends On      :[0m ttf-hack  ttf-joypixels
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m st
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 525.02 KiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sat 07 Nov 2020 01:15:53 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:15:59 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m stalonetray
[0;1mVersion         :[0m 0.8.3-3
[0;1mDescription     :[0m STAnd-aLONE sysTRAY. It has minimal build and run-time dependencies: the Xlib only.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://stalonetray.sourceforge.net
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxpm
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 140.89 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 04:57:13 PM MSK
[0;1mInstall Date    :[0m Wed 25 Nov 2020 07:36:55 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m steamcmd
[0;1mVersion         :[0m latest-3
[0;1mDescription     :[0m Steam Command Line Tools
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://developer.valvesoftware.com/wiki/SteamCMD
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m lib32-gcc-libs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 5.40 MiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sun 15 Nov 2020 02:37:20 AM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 07:13:35 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m None

[0;1mName            :[0m sublime-text
[0;1mVersion         :[0m 3211-1
[0;1mDescription     :[0m Sublime Text is a sophisticated text editor for code, markup and prose
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.sublimetext.com
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gtk3
[0;1mOptional Deps   :[0m gksu: sudo-save support
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m sublime-text-dev  sublime-text-nightly
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 33.23 MiB
[0;1mPackager        :[0m Sublime HQ Packager
[0;1mBuild Date      :[0m Tue 01 Oct 2019 04:00:48 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 06:59:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m sudo
[0;1mVersion         :[0m 1.9.4-2
[0;1mDescription     :[0m Give certain users the ability to run some commands as root
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.sudo.ws/sudo/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  libgcrypt  pam  libldap
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m yay-git
[0;1mOptional For    :[0m ranger
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 4.43 MiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Wed 09 Dec 2020 01:16:46 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:01 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m sxhkd
[0;1mVersion         :[0m 0.6.2-1
[0;1mDescription     :[0m Simple X hotkey daemon
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/baskerville/sxhkd
[0;1mLicenses        :[0m custom:BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xcb-util-keysyms
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 142.36 KiB
[0;1mPackager        :[0m Alexander Rødseth <rodseth@gmail.com>
[0;1mBuild Date      :[0m Fri 07 Aug 2020 03:44:09 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 10:58:04 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m sxiv-rifle
[0;1mVersion         :[0m 1.3-1
[0;1mDescription     :[0m Browse through images in directory after opening a single file
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://wiki.archlinux.org/index.php/Sxiv#Browse_through_images_in_directory_after_opening_a_single_file
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m sxiv
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1801.00 B
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Fri 20 Nov 2020 05:40:20 PM MSK
[0;1mInstall Date    :[0m Fri 20 Nov 2020 05:40:24 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m syncthing
[0;1mVersion         :[0m 1.12.0-1
[0;1mDescription     :[0m Open Source Continuous Replication / Cluster Synchronization Thing
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://syncthing.net/
[0;1mLicenses        :[0m MPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 26.10 MiB
[0;1mPackager        :[0m Jaroslav Lichtblau <svetlemodry@archlinux.org>
[0;1mBuild Date      :[0m Tue 01 Dec 2020 07:45:30 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:02 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m syntax-highlighting
[0;1mVersion         :[0m 5.76.0-1
[0;1mDescription     :[0m Syntax highlighting engine for structured text and code
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://community.kde.org/Frameworks
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m kf5
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m qt5-base
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 8.58 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Sun 08 Nov 2020 02:19:49 AM MSK
[0;1mInstall Date    :[0m Thu 19 Nov 2020 10:54:56 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m telegram-desktop
[0;1mVersion         :[0m 2.4.7-4
[0;1mDescription     :[0m Official Telegram Desktop client
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://desktop.telegram.org/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m hunspell  ffmpeg  hicolor-icon-theme  lz4  minizip  openal  qt5-imageformats  xxhash  libdbusmenu-qt5  qt5-wayland  gtk3
[0;1mOptional Deps   :[0m ttf-opensans: default Open Sans font family [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 60.87 MiB
[0;1mPackager        :[0m Jiachen YANG <farseerfc@gmail.com>
[0;1mBuild Date      :[0m Fri 11 Dec 2020 05:35:45 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:02 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m terminus-font
[0;1mVersion         :[0m 4.48-3
[0;1mDescription     :[0m Monospace bitmap font (for X11 and console)
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m http://terminus-font.sourceforge.net/
[0;1mLicenses        :[0m GPL2  custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1058.21 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Mon 29 Jun 2020 07:28:32 PM MSK
[0;1mInstall Date    :[0m Sat 14 Nov 2020 04:01:19 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m termite
[0;1mVersion         :[0m 15-3
[0;1mDescription     :[0m A simple VTE-based terminal
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/thestinger/termite/
[0;1mLicenses        :[0m LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gtk3  pcre2  gnutls  vte-common  termite-terminfo
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 612.23 KiB
[0;1mPackager        :[0m Frederik Schwan <freswa@archlinux.org>
[0;1mBuild Date      :[0m Wed 07 Oct 2020 10:37:05 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 09:29:48 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m testdisk
[0;1mVersion         :[0m 7.1-2
[0;1mDescription     :[0m Checks and undeletes partitions + PhotoRec, signature based recovery tool
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.cgsecurity.org/index.html?testdisk.html
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libjpeg  openssl  progsreiserfs  ntfsprogs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1605.67 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 16 May 2020 01:54:43 PM MSK
[0;1mInstall Date    :[0m Mon 07 Dec 2020 08:02:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m texinfo
[0;1mVersion         :[0m 6.7-3
[0;1mDescription     :[0m GNU documentation system for on-line information and printed output
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/texinfo/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ncurses  gzip  perl  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m guile
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 8.52 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Sun 26 Apr 2020 01:52:50 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:48 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m thunar
[0;1mVersion         :[0m 1.8.16-1
[0;1mDescription     :[0m Modern file manager for Xfce
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://docs.xfce.org/xfce/thunar/start
[0;1mLicenses        :[0m GPL2  LGPL2.1
[0;1mGroups          :[0m xfce4
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m desktop-file-utils  libexif  hicolor-icon-theme  libnotify  libgudev  exo  libxfce4util  libxfce4ui  libpng
[0;1mOptional Deps   :[0m gvfs: for trash support, mounting with udisk and remote filesystems [installed]
                  xfce4-panel: for trash applet [installed]
                  tumbler: for thumbnail previews
                  thunar-volman: manages removable devices
                  thunar-archive-plugin: create and deflate archives
                  thunar-media-tags-plugin: view/edit id3/ogg tags
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 6.44 MiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Sun 01 Nov 2020 06:07:36 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:26:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m thunderbird
[0;1mVersion         :[0m 78.4.3-1
[0;1mDescription     :[0m Standalone mail and news reader from mozilla.org
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.mozilla.org/thunderbird/
[0;1mLicenses        :[0m MPL  GPL  LGPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  gtk3  libgdk-3.so=0-64  mime-types  dbus  libdbus-1.so=3-64  dbus-glib  alsa-lib  nss  hunspell  sqlite  ttf-font  libvpx  libvpx.so=6-64  zlib  bzip2  botan  libwebp  libevent  libjpeg-turbo  libffi  nspr  gcc-libs  libx11  libxrender  libxfixes  libxext  libxcomposite  libxdamage  pango  libpango-1.0.so=0-64  cairo  gdk-pixbuf2  icu  libicui18n.so=67-64  libicuuc.so=67-64  freetype2  libfreetype.so=6-64  fontconfig  libfontconfig.so=1-64  glib2  libglib-2.0.so=0-64  pixman  libpixman-1.so=0-64  gnupg
[0;1mOptional Deps   :[0m libcanberra: sound support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 175.91 MiB
[0;1mPackager        :[0m Levente Polyak <anthraxx@archlinux.org>
[0;1mBuild Date      :[0m Wed 18 Nov 2020 09:20:24 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:18:00 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m tmux
[0;1mVersion         :[0m 3.1_c-1
[0;1mDescription     :[0m A terminal multiplexer
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/tmux/tmux/wiki
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ncurses  libevent  libutempter
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m fzf
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 763.06 KiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Fri 30 Oct 2020 03:16:48 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 09:30:02 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m tor
[0;1mVersion         :[0m 0.4.4.6-1
[0;1mDescription     :[0m Anonymizing overlay network.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.torproject.org/
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m openssl  libevent  bash  libseccomp  zstd  libcap  systemd-libs
[0;1mOptional Deps   :[0m torsocks: for torify
[0;1mRequired By     :[0m torbrowser-launcher
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 12.53 MiB
[0;1mPackager        :[0m Lukas Fleischer <lfleischer@archlinux.org>
[0;1mBuild Date      :[0m Sat 28 Nov 2020 04:59:58 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:37 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m torbrowser-launcher
[0;1mVersion         :[0m 0.3.2-7
[0;1mDescription     :[0m Securely and easily download, verify, install, and launch Tor Browser in Linux
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/micahflee/torbrowser-launcher
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python  python-pyqt5  python-requests  python-pysocks  python-gpgme  gnupg  tor
[0;1mOptional Deps   :[0m apparmor: support for apparmor profiles [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 164.26 KiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Thu 12 Nov 2020 08:22:30 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:02 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m transmission-cli
[0;1mVersion         :[0m 3.00-2
[0;1mDescription     :[0m Fast, easy, and free BitTorrent client (CLI tools, daemon and web client)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://www.transmissionbt.com/
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m curl  libevent  systemd
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m ranger  transmission-gtk  transmission-qt
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 4.16 MiB
[0;1mPackager        :[0m Florian Pritz <bluewind@xinu.at>
[0;1mBuild Date      :[0m Sun 26 Jul 2020 12:27:55 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 02:42:03 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m transmission-gtk
[0;1mVersion         :[0m 3.00-2
[0;1mDescription     :[0m Fast, easy, and free BitTorrent client (GTK+ GUI)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://www.transmissionbt.com/
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m curl  libevent  gtk3  desktop-file-utils  hicolor-icon-theme  libappindicator-gtk3
[0;1mOptional Deps   :[0m libnotify: Desktop notification support [installed]
                  transmission-cli: daemon and web support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.35 MiB
[0;1mPackager        :[0m Florian Pritz <bluewind@xinu.at>
[0;1mBuild Date      :[0m Sun 26 Jul 2020 12:27:55 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 02:42:03 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m transmission-qt
[0;1mVersion         :[0m 3.00-2
[0;1mDescription     :[0m Fast, easy, and free BitTorrent client (Qt GUI)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://www.transmissionbt.com/
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m curl  qt5-base  libevent
[0;1mOptional Deps   :[0m transmission-cli: daemon and web support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.82 MiB
[0;1mPackager        :[0m Florian Pritz <bluewind@xinu.at>
[0;1mBuild Date      :[0m Sun 26 Jul 2020 12:27:55 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 02:42:03 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m transmission-remote-gtk
[0;1mVersion         :[0m 1.4.1-2
[0;1mDescription     :[0m GTK remote control for the Transmission BitTorrent client
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/transmission-remote-gtk/transmission-remote-gtk
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m curl  gtk3  libnotify  libproxy  geoip
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 647.48 KiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Wed 08 Jul 2020 12:28:28 AM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 02:42:03 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m trayer
[0;1mVersion         :[0m 1.1.8-2
[0;1mDescription     :[0m lightweight GTK2-based systray for UNIX desktop
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/sargon/trayer-srg
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m trayer-srg
[0;1mDepends On      :[0m gtk2
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m trayer-srg
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 53.73 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 07:14:29 AM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 08:57:04 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-anonymous-pro
[0;1mVersion         :[0m 1.003-5
[0;1mDescription     :[0m A family of four fixed-width fonts designed especially with coding in mind
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.marksimonson.com/fonts/view/anonymous-pro
[0;1mLicenses        :[0m custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xorg-fonts-encodings
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1060.43 KiB
[0;1mPackager        :[0m Kyle Keen <keenerd@gmail.com>
[0;1mBuild Date      :[0m Mon 29 Jun 2020 05:21:31 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:42:39 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-bitstream-vera
[0;1mVersion         :[0m 1.10-14
[0;1mDescription     :[0m Bitstream Vera fonts.
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www-old.gnome.org/fonts/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m ttf-font
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m brave-bin  firefox  inkscape  qt5-webengine  thunderbird
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 570.63 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Mon 29 Jun 2020 03:10:04 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-cascadia-code
[0;1mVersion         :[0m 2009.22-1
[0;1mDescription     :[0m A monospaced font by Microsoft that includes programming ligatures
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/microsoft/cascadia-code
[0;1mLicenses        :[0m custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1396.45 KiB
[0;1mPackager        :[0m Konstantin Gizdov <arch@kge.pw>
[0;1mBuild Date      :[0m Fri 25 Sep 2020 11:58:21 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:42:39 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-eurof
[0;1mVersion         :[0m 1.0-2
[0;1mDescription     :[0m The original eurofurence font designed for headlines, signs, badges, inscriptions, et al.
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.dafont.com/eurofurence.font
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 972.08 KiB
[0;1mPackager        :[0m Frederik Schwan <freswa@archlinux.org>
[0;1mBuild Date      :[0m Mon 06 Jul 2020 04:53:45 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-fira-code
[0;1mVersion         :[0m 5.2-1
[0;1mDescription     :[0m Monospaced font with programming ligatures
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/tonsky/FiraCode
[0;1mLicenses        :[0m custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1772.00 KiB
[0;1mPackager        :[0m Jiachen YANG <farseerfc@archlinux.org>
[0;1mBuild Date      :[0m Wed 17 Jun 2020 05:03:49 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-fira-mono
[0;1mVersion         :[0m 2:3.206-4
[0;1mDescription     :[0m Mozilla's monospace typeface designed for Firefox OS
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/mozilla/Fira
[0;1mLicenses        :[0m custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 546.10 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Fri 26 Jun 2020 12:14:28 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-font-awesome
[0;1mVersion         :[0m 5.15.1-1
[0;1mDescription     :[0m Iconic font designed for Bootstrap
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://fontawesome.com/
[0;1mLicenses        :[0m custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 367.82 KiB
[0;1mPackager        :[0m Morten Linderud <foxboron@archlinux.org>
[0;1mBuild Date      :[0m Mon 05 Oct 2020 08:35:56 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-ibm-plex
[0;1mVersion         :[0m 5.1.3-1
[0;1mDescription     :[0m IBM Plex Mono, Sans, and Serif
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/IBM/plex
[0;1mLicenses        :[0m custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m ttf-font
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m brave-bin  firefox  inkscape  qt5-webengine  thunderbird
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m ibm-plex-fonts
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 34.86 MiB
[0;1mPackager        :[0m Santiago Torres-Arias <santiago@archlinux.org>
[0;1mBuild Date      :[0m Wed 09 Sep 2020 04:51:59 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-inconsolata
[0;1mVersion         :[0m 1:3.000-2
[0;1mDescription     :[0m Monospace font for pretty code listings and for the terminal
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.google.com/fonts/specimen/Inconsolata
[0;1mLicenses        :[0m custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 7.78 MiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Sat 28 Mar 2020 01:02:35 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-ionicons
[0;1mVersion         :[0m 5.2.3-1
[0;1mDescription     :[0m Font from the Ionic mobile framework
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://ionicons.com/
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 110.91 KiB
[0;1mPackager        :[0m Brett Cornwall <brett@i--b.com>
[0;1mBuild Date      :[0m Fri 09 Oct 2020 05:03:52 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-jetbrains-mono
[0;1mVersion         :[0m 2.210-1
[0;1mDescription     :[0m Typeface for developers, by JetBrains
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://jetbrains.com/lp/mono
[0;1mLicenses        :[0m custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.71 MiB
[0;1mPackager        :[0m Alexander Rødseth <rodseth@gmail.com>
[0;1mBuild Date      :[0m Tue 10 Nov 2020 02:28:37 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 04:47:02 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-linux-libertine
[0;1mVersion         :[0m 5.3.0-6
[0;1mDescription     :[0m Serif (Libertine) and Sans Serif (Biolinum) OpenType fonts with large Unicode coverage
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m http://www.linuxlibertine.org/
[0;1mLicenses        :[0m GPL  custom:OFL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 12.45 MiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Sun 30 Aug 2020 12:50:57 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-monofur
[0;1mVersion         :[0m 1.0-7
[0;1mDescription     :[0m A monospaced font derived from the eurofurence typeface family
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.dafont.com/monofur.font
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 343.77 KiB
[0;1mPackager        :[0m Frederik Schwan <freswa@archlinux.org>
[0;1mBuild Date      :[0m Mon 06 Jul 2020 04:47:15 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-nerd-fonts-symbols-mono
[0;1mVersion         :[0m 2.1.0+36+gd0bf73a1-2
[0;1mDescription     :[0m High number of extra glyphs from popular 'iconic fonts' (1000-em)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/ryanoasis/nerd-fonts
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m nerd-fonts
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m ttf-nerd-fonts-symbols
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 798.86 KiB
[0;1mPackager        :[0m Daniel M. Capella <polyzen@archlinux.org>
[0;1mBuild Date      :[0m Mon 13 Apr 2020 09:37:23 AM MSK
[0;1mInstall Date    :[0m Tue 08 Dec 2020 11:44:01 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-opensans
[0;1mVersion         :[0m 1.101-2
[0;1mDescription     :[0m Sans-serif typeface commissioned by Google
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://fonts.google.com/specimen/Open+Sans
[0;1mLicenses        :[0m Apache
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m telegram-desktop
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.74 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Mon 29 Jun 2020 01:49:33 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-roboto
[0;1mVersion         :[0m 2.138-4
[0;1mDescription     :[0m Google's signature family of fonts
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://material.google.com/style/typography.html
[0;1mLicenses        :[0m Apache
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 5.16 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Mon 29 Jun 2020 02:05:38 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ttf-ubuntu-font-family
[0;1mVersion         :[0m 0.83-6
[0;1mDescription     :[0m Ubuntu font family
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://design.ubuntu.com/font/
[0;1mLicenses        :[0m custom:Ubuntu Font Licence 1.0
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.95 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 06:50:05 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:44:40 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m ueberzug
[0;1mVersion         :[0m 18.1.8-1
[0;1mDescription     :[0m Command line util which allows to display images in combination with X11
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/seebye/ueberzug
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m python-ueberzug
[0;1mDepends On      :[0m libxext  python-attrs  python-docopt  python-pillow  python-psutil  python-setuptools  python-xlib
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m ranger
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m python-ueberzug
[0;1mInstalled Size  :[0m 331.13 KiB
[0;1mPackager        :[0m Daniel M. Capella <polyzen@archlinux.org>
[0;1mBuild Date      :[0m Thu 10 Dec 2020 01:35:56 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:02 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m unzip
[0;1mVersion         :[0m 6.0-14
[0;1mDescription     :[0m For extracting and viewing files in .zip archives
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.info-zip.org/UnZip.html
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m bzip2  bash
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m nnn
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 315.11 KiB
[0;1mPackager        :[0m Jelle van der Waa <jelle@archlinux.org>
[0;1mBuild Date      :[0m Fri 24 Apr 2020 04:00:39 PM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:03:27 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m usbutils
[0;1mVersion         :[0m 013-1
[0;1mDescription     :[0m A collection of USB tools to query connected USB devices
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://git.kernel.org/pub/scm/linux/kernel/git/gregkh/usbutils.git/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  hwids  libusb-1.0.so=0-64  libudev.so=1-64
[0;1mOptional Deps   :[0m python: for lsusb.py usage [installed]
                  coreutils: for lsusb.py usage [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 313.86 KiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Thu 12 Nov 2020 01:36:38 PM MSK
[0;1mInstall Date    :[0m Thu 19 Nov 2020 10:54:58 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m vifm
[0;1mVersion         :[0m 0.11-1
[0;1mDescription     :[0m A file manager with curses interface, which provides Vi[m]-like environment
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://vifm.info/
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ncurses  desktop-file-utils
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.15 MiB
[0;1mPackager        :[0m Jaroslav Lichtblau <svetlemodry@archlinux.org>
[0;1mBuild Date      :[0m Wed 07 Oct 2020 10:22:04 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:36:04 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m vim
[0;1mVersion         :[0m 8.2.1989-3
[0;1mDescription     :[0m Vi Improved, a highly configurable, improved version of the vi text editor
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.vim.org
[0;1mLicenses        :[0m custom:vim
[0;1mGroups          :[0m None
[0;1mProvides        :[0m xxd  vim-minimal  vim-python3  vim-plugin-runtime
[0;1mDepends On      :[0m vim-runtime=8.2.1989-3  gpm  acl  glibc  libgcrypt  pcre  zlib  libffi
[0;1mOptional Deps   :[0m python2: Python 2 language support [installed]
                  python: Python 3 language support [installed]
                  ruby: Ruby language support [installed]
                  lua: Lua language support [installed]
                  perl: Perl language support [installed]
                  tcl: Tcl language support
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m fzf
[0;1mConflicts With  :[0m gvim  vim-minimal  vim-python3
[0;1mReplaces        :[0m vim-python3  vim-minimal
[0;1mInstalled Size  :[0m 3.75 MiB
[0;1mPackager        :[0m Levente Polyak <anthraxx@archlinux.org>
[0;1mBuild Date      :[0m Sun 15 Nov 2020 07:46:37 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:03 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m visual-studio-code-bin
[0;1mVersion         :[0m 1.51.1-1
[0;1mDescription     :[0m Visual Studio Code (vscode): Editor for building and debugging modern web and cloud applications (official binary version)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://code.visualstudio.com/
[0;1mLicenses        :[0m custom: commercial
[0;1mGroups          :[0m None
[0;1mProvides        :[0m code
[0;1mDepends On      :[0m libxkbfile  gnupg  gtk3  libsecret  nss  gcc-libs  libnotify  libxss  glibc  lsof
[0;1mOptional Deps   :[0m glib2: Needed for move to trash functionality [installed]
                  libdbusmenu-glib: Needed for KDE global menu [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m code
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 243.19 MiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Fri 13 Nov 2020 06:31:25 AM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 06:33:52 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m vlc
[0;1mVersion         :[0m 3.0.11.1-6
[0;1mDescription     :[0m Multi-platform MPEG, VCD/DVD, and DivX player
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.videolan.org/vlc/
[0;1mLicenses        :[0m LGPL2.1  GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m a52dec  libdvbpsi  libxpm  libdca  libproxy  lua52  libidn  libmatroska  taglib  libmpcdec  ffmpeg  faad2  libmad  libmpeg2  xcb-util-keysyms  libtar  libxinerama  libsecret  libupnp  libixml.so=11-64  libupnp.so=17-64  libarchive  qt5-base  qt5-x11extras  qt5-svg  freetype2  fribidi  harfbuzz  fontconfig  libxml2  gnutls  libplacebo  wayland-protocols
[0;1mOptional Deps   :[0m avahi: service discovery using bonjour protocol [installed]
                  aom: AOM AV1 codec [installed]
                  gst-plugins-base-libs: for libgst plugins [installed]
                  dav1d: dav1d AV1 decoder [installed]
                  libdvdcss: decoding encrypted DVDs
                  libavc1394: devices using the 1394ta AV/C [installed]
                  libdc1394: IEEE 1394 access plugin
                  kwallet: kwallet keystore [installed]
                  libva-vdpau-driver: vdpau backend nvidia [installed]
                  libva-intel-driver: video backend intel
                  libbluray: Blu-Ray video input [installed]
                  flac: Free Lossless Audio Codec plugin [installed]
                  twolame: TwoLAME mpeg2 encoder plugin [installed]
                  libgme: Game Music Emu plugin [installed]
                  vcdimager: navigate VCD with libvcdinfo
                  libmtp: MTP devices discovery
                  systemd-libs: udev services discovery [installed]
                  smbclient: SMB access plugin [installed]
                  libcdio: audio CD playback [installed]
                  gnu-free-fonts: subtitle font  [installed]
                  ttf-dejavu: subtitle font
                  libssh2: sftp access [installed]
                  libnfs: NFS access [installed]
                  mpg123: mpg123 codec [installed]
                  protobuf: chromecast streaming [installed]
                  libmicrodns: mDNS services discovery (chromecast etc)
                  lua52-socket: http interface [installed]
                  live-media: RTSP input
                  libdvdread: DVD input module [installed]
                  libdvdnav: DVD with navigation input module [installed]
                  libogg: Ogg and OggSpots codec [installed]
                  libshout: shoutcast/icecast output plugin [installed]
                  libmodplug: MOD output plugin [installed]
                  libvpx: VP8 and VP9 codec [installed]
                  libvorbis: Vorbis decoder/encoder [installed]
                  speex: Speex codec [installed]
                  opus: opus codec [installed]
                  libtheora: theora codec [installed]
                  libpng: PNG support [installed]
                  libjpeg-turbo: JPEG support [installed]
                  librsvg: SVG plugin [installed]
                  x264: H264 encoding [installed]
                  x265: HEVC/H.265 encoder [installed]
                  zvbi: VBI/Teletext/webcam/v4l2 capture/decoding
                  libass: Subtitle support [installed]
                  libkate: Kate codec
                  libtiger: Tiger rendering for Kate streams
                  sdl_image: SDL image support
                  srt: SRT input/output plugin [installed]
                  aalib: ASCII art video output
                  libcaca: colored ASCII art video output [installed]
                  libpulse: PulseAudio audio output [installed]
                  alsa-lib: ALSA audio output [installed]
                  jack: jack audio server [installed]
                  libsamplerate: audio Resampler [installed]
                  libsoxr: SoX audio Resampler [installed]
                  chromaprint: Chromaprint audio fingerprinter [installed]
                  lirc: lirc control
                  libgoom2: Goom visualization
                  projectm: ProjectM visualisation
                  ncurses: ncurses interface [installed]
                  libnotify: notification plugin [installed]
                  gtk3: notification plugin [installed]
                  aribb24: aribsub support [installed]
                  aribb25: aribcam support
                  pcsclite: aribcam support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m obs-studio
[0;1mConflicts With  :[0m vlc-plugin
[0;1mReplaces        :[0m vlc-plugin
[0;1mInstalled Size  :[0m 57.87 MiB
[0;1mPackager        :[0m Levente Polyak <anthraxx@archlinux.org>
[0;1mBuild Date      :[0m Tue 10 Nov 2020 11:47:37 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 04:47:06 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m vtop
[0;1mVersion         :[0m 0.6.1-1
[0;1mDescription     :[0m Wow such top. So stats
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m http://parall.ax/vtop
[0;1mLicenses        :[0m None
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m nodejs
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 5.17 MiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Mon 30 Nov 2020 02:33:45 AM MSK
[0;1mInstall Date    :[0m Mon 30 Nov 2020 02:34:05 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m vulkan-extra-layers
[0;1mVersion         :[0m 1.2.154.0-1
[0;1mDescription     :[0m Extra layers for Vulkan development
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://lunarg.com/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m vulkan-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m vulkan-validation-layers
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 8.51 MiB
[0;1mPackager        :[0m Laurent Carlier <lordheavym@gmail.com>
[0;1mBuild Date      :[0m Tue 03 Nov 2020 01:00:36 PM MSK
[0;1mInstall Date    :[0m Mon 16 Nov 2020 01:51:39 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m vulkan-extra-tools
[0;1mVersion         :[0m 1.2.154.0-1
[0;1mDescription     :[0m Vulkan lunarg tools
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m http://lunarg.com/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m vulkan-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m vulkan-validation-layers  qt5-svg  qt5-webengine
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m vulkan-trace<1.2.148.1
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1504.19 KiB
[0;1mPackager        :[0m Laurent Carlier <lordheavym@gmail.com>
[0;1mBuild Date      :[0m Tue 03 Nov 2020 01:00:36 PM MSK
[0;1mInstall Date    :[0m Mon 16 Nov 2020 01:51:40 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m vulkan-headers
[0;1mVersion         :[0m 1:1.2.164-1
[0;1mDescription     :[0m Vulkan header files
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://www.khronos.org/vulkan/
[0;1mLicenses        :[0m APACHE
[0;1mGroups          :[0m vulkan-devel
[0;1mProvides        :[0m vulkan-hpp=1.2.164
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m vulkan-validation-layers
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 9.21 MiB
[0;1mPackager        :[0m Laurent Carlier <lordheavym@gmail.com>
[0;1mBuild Date      :[0m Fri 11 Dec 2020 12:27:00 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:03 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m vulkan-intel
[0;1mVersion         :[0m 20.3.0-3
[0;1mDescription     :[0m Intel's Vulkan mesa driver
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.mesa3d.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m vulkan-driver
[0;1mDepends On      :[0m wayland  libx11  libxshmfence  libdrm  zstd
[0;1mOptional Deps   :[0m vulkan-mesa-layers: additional vulkan layers
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m vulkan-icd-loader
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 7.44 MiB
[0;1mPackager        :[0m Christian Hesse <arch@eworm.de>
[0;1mBuild Date      :[0m Tue 08 Dec 2020 11:06:32 AM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:03 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m vulkan-tools
[0;1mVersion         :[0m 1.2.162-1
[0;1mDescription     :[0m Vulkan Utilities and Tools
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.khronos.org/vulkan/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m vulkan-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gcc-libs  libxcb  vulkan-icd-loader
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1084.78 KiB
[0;1mPackager        :[0m Laurent Carlier <lordheavym@gmail.com>
[0;1mBuild Date      :[0m Fri 11 Dec 2020 12:39:55 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:03 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m w3m
[0;1mVersion         :[0m 0.5.3.git20200507-2
[0;1mDescription     :[0m Text-based Web browser as well as pager
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://salsa.debian.org/debian/w3m
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m openssl  gc  ncurses  gpm
[0;1mOptional Deps   :[0m imlib2: for graphics support [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m neofetch  ranger
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1968.79 KiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Sun 30 Aug 2020 12:51:18 PM MSK
[0;1mInstall Date    :[0m Fri 13 Nov 2020 08:06:00 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m wget
[0;1mVersion         :[0m 1.20.3-3
[0;1mDescription     :[0m Network utility to retrieve files from the Web
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.gnu.org/software/wget/wget.html
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  gnutls  libidn2  libutil-linux  libpsl  pcre2
[0;1mOptional Deps   :[0m ca-certificates: HTTPS downloads [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.88 MiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Tue 05 May 2020 08:28:21 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:33:41 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m which
[0;1mVersion         :[0m 2.21-5
[0;1mDescription     :[0m A utility to show the full path of commands
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://savannah.gnu.org/projects/which/
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m base-devel
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  bash
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m netdata  xdg-utils
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 34.38 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Wed 13 Nov 2019 08:08:36 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:03:49 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m wifite
[0;1mVersion         :[0m 1:87.r139.918a499-3
[0;1mDescription     :[0m Tool to attack multiple WEP and WPA encrypted networks at the same time
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/derv82/wifite
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m python2  aircrack-ng
[0;1mOptional Deps   :[0m reaver: WPS attack support
                  pyrit: detect WPA handshakes
                  wireshark-cli: detect handshakes
                  cowpatty: detect WPA handshakes
                  macchanger: change MAC for attacks
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 160.82 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 09:06:15 PM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:40:21 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xf86-video-fbdev
[0;1mVersion         :[0m 0.5.0-2
[0;1mDescription     :[0m X.org framebuffer video driver
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-drivers
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m xorg-server<1.20  X-ABI-VIDEODRV_VERSION<24  X-ABI-VIDEODRV_VERSION>=25
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 29.26 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 16 May 2020 12:06:46 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:54 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xf86-video-vesa
[0;1mVersion         :[0m 2.5.0-1
[0;1mDescription     :[0m X.org vesa video driver
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-drivers  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m xorg-server<1.20  X-ABI-VIDEODRV_VERSION<24  X-ABI-VIDEODRV_VERSION>=25
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 33.15 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Wed 23 Sep 2020 09:50:03 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:54 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xfce4-battery-plugin
[0;1mVersion         :[0m 1.1.3-2
[0;1mDescription     :[0m A battery monitor plugin for the Xfce panel
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://docs.xfce.org/panel-plugins/xfce4-battery-plugin
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m xfce4-goodies
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xfce4-panel
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 459.05 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 19 May 2020 11:46:07 AM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 05:57:14 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xfce4-power-manager
[0;1mVersion         :[0m 1.6.6-1
[0;1mDescription     :[0m Power manager for Xfce desktop
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.xfce.org/
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m xfce4
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxfce4ui  upower  libnotify  hicolor-icon-theme
[0;1mOptional Deps   :[0m xfce4-panel: for the Xfce panel plugin [installed]
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 3.71 MiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Mon 09 Mar 2020 01:08:48 AM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 11:32:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xfce4-screenshooter
[0;1mVersion         :[0m 1.9.7-2
[0;1mDescription     :[0m Plugin that makes screenshots for the Xfce panel
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://docs.xfce.org/apps/screenshooter/start
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m xfce4-goodies
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xfce4-panel  libsoup  hicolor-icon-theme
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m xfce4-screenshooter-plugin
[0;1mReplaces        :[0m xfce4-screenshooter-plugin
[0;1mInstalled Size  :[0m 670.81 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 16 May 2020 02:00:52 PM MSK
[0;1mInstall Date    :[0m Wed 09 Dec 2020 02:21:05 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xfce4-sensors-plugin
[0;1mVersion         :[0m 1.3.92-2
[0;1mDescription     :[0m A lm_sensors plugin for the Xfce panel
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://docs.xfce.org/panel-plugins/xfce4-sensors-plugin
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m xfce4-goodies
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xfce4-panel  lm_sensors  libnotify  hicolor-icon-theme
[0;1mOptional Deps   :[0m hddtemp: for monitoring the temperature of hard drives
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 525.88 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 19 May 2020 01:08:59 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:26:47 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xmobar
[0;1mVersion         :[0m 0.36-47
[0;1mDescription     :[0m Minimalistic Text Based Status Bar
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://hackage.haskell.org/package/xmobar
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxft  libxinerama  libxrandr  libxpm  ghc-libs  haskell-x11  haskell-x11-xft  haskell-utf8-string  haskell-network-uri  haskell-hinotify  haskell-stm  haskell-parsec  haskell-parsec-numbers  haskell-mtl  haskell-regex-base  haskell-regex-compat  haskell-http  haskell-dbus  haskell-libmpd  haskell-cereal  haskell-netlink  haskell-text  haskell-async  haskell-aeson  haskell-timezone-olson  haskell-timezone-series  alsa-lib  haskell-extensible-exceptions  haskell-http-conduit  haskell-http-types  haskell-http-client-tls  haskell-alsa-core  haskell-alsa-mixer
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 4.86 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Wed 09 Dec 2020 02:35:32 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:18 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xmonad
[0;1mVersion         :[0m 0.15-74
[0;1mDescription     :[0m Lightweight X11 tiled window manager written in Haskell
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xmonad.org/
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ghc  haskell-x11  sh  haskell-utf8-string  haskell-extensible-exceptions  haskell-data-default  haskell-setlocale
[0;1mOptional Deps   :[0m xorg-xmessage: for displaying visual error messages
[0;1mRequired By     :[0m xmonad-contrib
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1489.72 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Fri 27 Nov 2020 05:55:41 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:18 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xmonad-contrib
[0;1mVersion         :[0m 0.16-54
[0;1mDescription     :[0m Add-ons for xmonad
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xmonad.org/
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m ghc-libs  xmonad  sh  haskell-x11  haskell-x11-xft  haskell-utf8-string  haskell-random  haskell-old-time
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 17.41 MiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Fri 27 Nov 2020 05:57:39 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:18 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-bdftopcf
[0;1mVersion         :[0m 1.1-2
[0;1mDescription     :[0m Convert X font from Bitmap Distribution Format to Portable Compiled Format
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg  xorg-apps
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 44.73 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 06:13:20 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:54 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-docs
[0;1mVersion         :[0m 1.7.1-3
[0;1mDescription     :[0m X.org documentations
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m man-db
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 838.29 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:23:30 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-font-util
[0;1mVersion         :[0m 1.3.2-2
[0;1mDescription     :[0m X.Org font utilities
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-fonts  xorg
[0;1mProvides        :[0m font-util
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m font-util
[0;1mReplaces        :[0m font-util
[0;1mInstalled Size  :[0m 230.95 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 05:09:16 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-fonts-100dpi
[0;1mVersion         :[0m 1.0.3-7
[0;1mDescription     :[0m X.org 100dpi fonts
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xorg-fonts-alias-100dpi
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 12.18 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Tue 30 Jun 2020 08:00:36 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-fonts-75dpi
[0;1mVersion         :[0m 1.0.3-7
[0;1mDescription     :[0m X.org 75dpi fonts
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xorg-fonts-alias-75dpi
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 10.64 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Tue 30 Jun 2020 08:07:12 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-fonts-encodings
[0;1mVersion         :[0m 1.0.5-2
[0;1mDescription     :[0m X.org font encoding files
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-fonts  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m libfontenc  ttf-anonymous-pro
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 625.44 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 02:50:06 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-iceauth
[0;1mVersion         :[0m 1.0.8-2
[0;1mDescription     :[0m ICE authority file utility
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libice
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 40.68 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 06:10:33 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-mkfontscale
[0;1mVersion         :[0m 1.2.1-2
[0;1mDescription     :[0m Create an index of scalable font files for X
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m xorg-mkfontdir
[0;1mDepends On      :[0m freetype2  libfontenc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m xorg-mkfontdir
[0;1mReplaces        :[0m xorg-mkfontdir
[0;1mInstalled Size  :[0m 51.58 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Sun 19 Jan 2020 01:35:53 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-oclock
[0;1mVersion         :[0m 1.0.4-2
[0;1mDescription     :[0m Round X clock
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxmu  libxext  libxkbfile
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 31.15 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 06:08:34 AM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-server
[0;1mVersion         :[0m 1.20.10-2
[0;1mDescription     :[0m Xorg X server
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m X-ABI-VIDEODRV_VERSION=24.0  X-ABI-XINPUT_VERSION=24.1  X-ABI-EXTENSION_VERSION=10.0  x-server
[0;1mDepends On      :[0m libepoxy  libxfont2  pixman  xorg-server-common  libunwind  dbus  libgl  xf86-input-libinput  nettle  libpciaccess  libdrm  libxshmfence
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m nvidia-utils
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m nvidia-utils<=331.20  glamor-egl  xf86-video-modesetting
[0;1mReplaces        :[0m glamor-egl  xf86-video-modesetting
[0;1mInstalled Size  :[0m 3.65 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Thu 10 Dec 2020 03:04:42 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:12:51 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-server-common
[0;1mVersion         :[0m 1.20.10-2
[0;1mDescription     :[0m Xorg server common files
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xkeyboard-config  xorg-xkbcomp  xorg-setxkbmap
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m xorg-server  xorg-server-xephyr  xorg-server-xnest  xorg-server-xvfb  xorg-server-xwayland
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 127.21 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Thu 10 Dec 2020 03:04:42 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:12:45 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-server-devel
[0;1mVersion         :[0m 1.20.10-2
[0;1mDescription     :[0m Development files for the X.Org X server
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xorgproto  mesa  libpciaccess  xorg-util-macros
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m nvidia-utils
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1240.97 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Thu 10 Dec 2020 03:04:42 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:18 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-server-xephyr
[0;1mVersion         :[0m 1.20.10-2
[0;1mDescription     :[0m A nested X server that runs as an X application
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxfont2  libgl  libepoxy  libunwind  systemd-libs  libxv  pixman  xorg-server-common  xcb-util-image  xcb-util-renderutil  xcb-util-wm  xcb-util-keysyms  nettle  libtirpc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.19 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Thu 10 Dec 2020 03:04:42 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:18 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-server-xnest
[0;1mVersion         :[0m 1.20.10-2
[0;1mDescription     :[0m A nested X server that runs as an X application
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxfont2  libxext  pixman  xorg-server-common  nettle  libtirpc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1412.96 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Thu 10 Dec 2020 03:04:42 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:18 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-server-xvfb
[0;1mVersion         :[0m 1.20.10-2
[0;1mDescription     :[0m Virtual framebuffer X server
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxfont2  libunwind  pixman  xorg-server-common  xorg-xauth  libgl  nettle  libtirpc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1887.66 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Thu 10 Dec 2020 03:04:42 PM MSK
[0;1mInstall Date    :[0m Fri 11 Dec 2020 02:16:18 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-server-xwayland
[0;1mVersion         :[0m 1.20.9.r21.g5c400cae1-2
[0;1mDescription     :[0m run X clients under wayland
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxfont2  libepoxy  libunwind  systemd-libs  libgl  pixman  xorg-server-common  nettle  libtirpc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.12 MiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Thu 19 Nov 2020 02:42:45 PM MSK
[0;1mInstall Date    :[0m Sat 28 Nov 2020 08:24:38 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-sessreg
[0;1mVersion         :[0m 1.1.2-2
[0;1mDescription     :[0m Register X sessions in system utmp/utmpx databases
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 17.73 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 05:32:56 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-setxkbmap
[0;1mVersion         :[0m 1.3.2-2
[0;1mDescription     :[0m Set the keyboard using the X Keyboard Extension
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxkbfile
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m xorg-server-common
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 29.09 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:26:09 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-smproxy
[0;1mVersion         :[0m 1.0.6-3
[0;1mDescription     :[0m Allows X applications that do not support X11R6 session management to participate in an X11R6 session
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libsm  libxt  libxmu
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 28.19 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:26:47 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-x11perf
[0;1mVersion         :[0m 1.6.1-2
[0;1mDescription     :[0m Simple X server performance benchmarker
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxmu  libxrender  libxft  libxext
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 234.40 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 05:03:54 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xauth
[0;1mVersion         :[0m 1.1-2
[0;1mDescription     :[0m X.Org authorization settings program
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxmu
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m xorg-server-xvfb  xorg-xinit
[0;1mOptional For    :[0m openssh
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 55.85 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 01:37:17 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xbacklight
[0;1mVersion         :[0m 1.2.3-2
[0;1mDescription     :[0m RandR-based backlight control application
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m xcb-util
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 16.06 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Wed 06 May 2020 06:15:21 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xclipboard
[0;1mVersion         :[0m 1.1.3-3
[0;1mDescription     :[0m X clipboard manager
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxaw  libxmu  libxt  libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 57.39 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 16 May 2020 02:28:52 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xclock
[0;1mVersion         :[0m 1.0.9-2
[0;1mDescription     :[0m X clock
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxmu  libxaw  libxrender  libxft  libxkbfile
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 67.42 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:55:45 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xcmsdb
[0;1mVersion         :[0m 1.0.5-3
[0;1mDescription     :[0m Device Color Characterization utility for X Color Management System
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 42.42 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:53:25 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xcursorgen
[0;1mVersion         :[0m 1.0.7-2
[0;1mDescription     :[0m Create an X cursor file from PNG images
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxcursor  libpng
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 20.15 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:48:59 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xdpyinfo
[0;1mVersion         :[0m 1.3.2-4
[0;1mDescription     :[0m Display information utility for X
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxext  libxtst  libxxf86vm  libxrender  libxcomposite  libxinerama
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m neofetch
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 33.04 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:44:48 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xdriinfo
[0;1mVersion         :[0m 1.0.6-2
[0;1mDescription     :[0m Query configuration information of DRI drivers
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libgl
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 15.42 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 06:02:28 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xev
[0;1mVersion         :[0m 1.2.4-1
[0;1mDescription     :[0m Print contents of X events
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxrandr
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 32.54 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Sun 19 Jul 2020 12:27:33 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xfd
[0;1mVersion         :[0m 1.1.3-2
[0;1mDescription     :[0m Displays all the characters in a font using either the X11 core protocol or libXft2
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxaw  fontconfig  libxft  libxrender  libxmu  libxkbfile
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 40.58 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:24:51 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:47:17 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xfontsel
[0;1mVersion         :[0m 1.0.6-3
[0;1mDescription     :[0m Point and click selection of X11 font names
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxaw  libxmu  libxt  libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 58.03 KiB
[0;1mPackager        :[0m Evangelos Foutras <foutrelis@archlinux.org>
[0;1mBuild Date      :[0m Sun 30 Aug 2020 12:51:27 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xgamma
[0;1mVersion         :[0m 1.0.6-3
[0;1mDescription     :[0m Alter a monitor's gamma correction
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxxf86vm
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 17.62 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:46:23 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xhost
[0;1mVersion         :[0m 1.0.8-2
[0;1mDescription     :[0m Server access control program for X
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxmu
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 23.56 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:56:54 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xinit
[0;1mVersion         :[0m 1.4.1-3
[0;1mDescription     :[0m X.Org initialisation program
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  xorg-xauth  xorg-xrdb  xorg-xmodmap
[0;1mOptional Deps   :[0m xorg-twm
                  xterm
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 35.42 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Sat 10 Oct 2020 05:27:47 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xinput
[0;1mVersion         :[0m 1.6.3-2
[0;1mDescription     :[0m Small commandline tool to configure devices
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxi>=1.5.99.2  xorg-xrandr  libxinerama
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 63.81 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 16 May 2020 01:03:55 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xkbcomp
[0;1mVersion         :[0m 1.4.4-1
[0;1mDescription     :[0m X Keyboard description compiler
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxkbfile
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m xorg-server-common
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 221.83 KiB
[0;1mPackager        :[0m Andreas Radke <andyrtr@archlinux.org>
[0;1mBuild Date      :[0m Thu 05 Nov 2020 09:50:09 AM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xkbevd
[0;1mVersion         :[0m 1.1.4-3
[0;1mDescription     :[0m XKB event daemon
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxkbfile
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 44.79 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:34:58 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xkbutils
[0;1mVersion         :[0m 1.0.4-4
[0;1mDescription     :[0m XKB utility demos
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxaw  libxt  libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 65.46 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Sat 16 May 2020 02:05:24 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xkill
[0;1mVersion         :[0m 1.0.5-2
[0;1mDescription     :[0m Kill a client by its X resource
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxmu
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 16.64 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 05:55:34 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xlsatoms
[0;1mVersion         :[0m 1.1.3-2
[0;1mDescription     :[0m List interned atoms defined on server
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxcb
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 16.24 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Wed 06 May 2020 06:13:27 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xlsclients
[0;1mVersion         :[0m 1.1.4-2
[0;1mDescription     :[0m List client applications running on a display
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxcb
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 21.19 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Wed 06 May 2020 06:11:36 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xmodmap
[0;1mVersion         :[0m 1.0.10-2
[0;1mDescription     :[0m Utility for modifying keymaps and button mappings
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m xorg-xinit
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 49.09 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 05:10:15 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xpr
[0;1mVersion         :[0m 1.0.5-2
[0;1mDescription     :[0m Print an X window dump from xwd
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxmu  sh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 64.84 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 05:53:44 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xrandr
[0;1mVersion         :[0m 1.5.1-2
[0;1mDescription     :[0m Primitive command line interface to RandR extension
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxrandr  libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m xorg-xinput
[0;1mOptional For    :[0m neofetch
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 69.44 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 12:41:39 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xrdb
[0;1mVersion         :[0m 1.2.0-2
[0;1mDescription     :[0m X server resource database utility
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxmu
[0;1mOptional Deps   :[0m gcc: for preprocessing [installed]
                  mcpp: a lightweight alternative for preprocessing
[0;1mRequired By     :[0m xorg-xinit
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 40.58 KiB
[0;1mPackager        :[0m Evangelos Foutras <evangelos@foutrelis.com>
[0;1mBuild Date      :[0m Sat 16 May 2020 04:47:16 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xrefresh
[0;1mVersion         :[0m 1.0.6-2
[0;1mDescription     :[0m Refresh all or part of an X screen
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 17.55 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 05:52:34 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xsetroot
[0;1mVersion         :[0m 1.1.2-2
[0;1mDescription     :[0m Classic X utility to set your root window background to a given pattern or color
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxmu  libx11  libxcursor
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 21.07 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 05:49:49 AM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 06:13:03 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xvinfo
[0;1mVersion         :[0m 1.1.4-2
[0;1mDescription     :[0m Prints out the capabilities of any video adaptors associated with the display that are accessible through the X-Video extension
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11  libxv
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 19.59 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Fri 15 May 2020 05:47:20 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xwd
[0;1mVersion         :[0m 1.0.7-2
[0;1mDescription     :[0m X Window System image dumping utility
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxkbfile
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 38.04 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 05:46:57 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xwininfo
[0;1mVersion         :[0m 1.1.5-2
[0;1mDescription     :[0m Command-line utility to print information about windows on an X server
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libxcb
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m neofetch  picom
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 52.19 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Fri 15 May 2020 04:59:36 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xorg-xwud
[0;1mVersion         :[0m 1.0.5-2
[0;1mDescription     :[0m X Window System image undumping utility
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://xorg.freedesktop.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m xorg-apps  xorg
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libx11
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 37.30 KiB
[0;1mPackager        :[0m Allan McRae <allan@archlinux.org>
[0;1mBuild Date      :[0m Thu 07 May 2020 05:45:13 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:59:57 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xscreensaver
[0;1mVersion         :[0m 5.44-3
[0;1mDescription     :[0m Screen saver and locker for the X Window System
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.jwz.org/xscreensaver/
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libglade  libxmu  glu  xorg-appres  perl-libwww  libsystemd.so=0-64  libxcrypt  libcrypt.so=2-64  libxi  libxxf86vm  libxrandr  libxinerama  libxt  libx11  libxext  pam  glibc  gdk-pixbuf-xlib
[0;1mOptional Deps   :[0m gdm: for login manager support
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 33.24 MiB
[0;1mPackager        :[0m Jan Alexander Steffens (heftig) <heftig@archlinux.org>
[0;1mBuild Date      :[0m Mon 09 Nov 2020 05:50:04 PM MSK
[0;1mInstall Date    :[0m Tue 17 Nov 2020 03:42:54 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m xsensors
[0;1mVersion         :[0m 0.80-3
[0;1mDescription     :[0m X11 interface to lm_sensors - Mystro256 fork
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/Mystro256/xsensors
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m gtk3  lm_sensors
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 72.29 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 07:52:26 PM MSK
[0;1mInstall Date    :[0m Sun 29 Nov 2020 06:29:10 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m yay-git
[0;1mVersion         :[0m 10.1.0.r9.gde7373d-1
[0;1mDescription     :[0m Yet another yogurt. Pacman wrapper and AUR helper written in go. (development version)
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/Jguer/yay
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m yay
[0;1mDepends On      :[0m libalpm.so>=12  git  sudo
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m yay
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 7.97 MiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sat 07 Nov 2020 01:10:22 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 01:11:55 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m youtube
[0;1mVersion         :[0m 1.0.3-4
[0;1mDescription     :[0m Unnofficial Youtube desktop application
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://gitlab.com/youtube-desktop/application
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m youtube
[0;1mDepends On      :[0m nss  gtk3  libxss
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m youtube
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 179.42 MiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sun 15 Nov 2020 02:22:20 AM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:24:36 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m youtube-cli
[0;1mVersion         :[0m 1.2-1
[0;1mDescription     :[0m A cli client to play audio from youtube with caching
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/uditkarode/youtube-cli
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m youtube-dl  mpv  git  python
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 2.49 KiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sun 15 Nov 2020 02:22:14 AM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:22:17 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m youtube-desktop
[0;1mVersion         :[0m 0.91-1
[0;1mDescription     :[0m YouTube web-app based on qtws
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m None
[0;1mLicenses        :[0m GPL3
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m qtws-base
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 9.10 KiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sun 15 Nov 2020 02:22:07 AM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:22:10 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m youtube-mpv-git
[0;1mVersion         :[0m 69-1
[0;1mDescription     :[0m Browser extension that adds context menu option to play youtube (and other youtube-dl supported) videos with mpv.
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/agiz/youtube-mpv
[0;1mLicenses        :[0m GPL2
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m youtube-dl  python
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 53.63 KiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sun 15 Nov 2020 02:24:39 AM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:24:43 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m youtube-tool
[0;1mVersion         :[0m 1.0.5-1
[0;1mDescription     :[0m CLI tool to extract comments, subtitles or livechat content from a youtube video
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/nlitsme/youtube_tool
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m yttool
[0;1mDepends On      :[0m python
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 87.10 KiB
[0;1mPackager        :[0m Unknown Packager
[0;1mBuild Date      :[0m Sun 15 Nov 2020 02:24:46 AM MSK
[0;1mInstall Date    :[0m Sun 15 Nov 2020 02:24:50 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m None

[0;1mName            :[0m zathura
[0;1mVersion         :[0m 0.4.7-1
[0;1mDescription     :[0m Minimalistic document viewer
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://pwmt.org/projects/zathura/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m girara>=0.2.7  sqlite  desktop-file-utils  file  libsynctex
[0;1mOptional Deps   :[0m zathura-djvu: DjVu support [installed]
                  zathura-pdf-poppler: PDF support using Poppler [installed]
                  zathura-pdf-mupdf: PDF support using MuPDF
                  zathura-ps: PostScript support [installed]
                  zathura-cb: Comic book support
[0;1mRequired By     :[0m zathura-djvu  zathura-pdf-poppler  zathura-ps
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 595.87 KiB
[0;1mPackager        :[0m Johannes Löthberg <demize@archlinux.org>
[0;1mBuild Date      :[0m Sat 10 Oct 2020 05:27:06 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:36:04 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zathura-djvu
[0;1mVersion         :[0m 0.2.9-1
[0;1mDescription     :[0m DjVu support for Zathura
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://pwmt.org/projects/zathura-djvu/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m djvulibre  zathura  desktop-file-utils
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m zathura
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 27.23 KiB
[0;1mPackager        :[0m Johannes Löthberg <johannes@kyriasis.com>
[0;1mBuild Date      :[0m Sat 01 Feb 2020 03:55:13 PM MSK
[0;1mInstall Date    :[0m Wed 25 Nov 2020 03:36:14 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zathura-pdf-poppler
[0;1mVersion         :[0m 0.3.0-1
[0;1mDescription     :[0m Adds pdf support to zathura by using the poppler engine
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://pwmt.org/projects/zathura-pdf-poppler/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m poppler-glib  zathura  desktop-file-utils
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m zathura
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 23.22 KiB
[0;1mPackager        :[0m Johannes Löthberg <johannes@kyriasis.com>
[0;1mBuild Date      :[0m Sat 01 Feb 2020 03:55:56 PM MSK
[0;1mInstall Date    :[0m Wed 25 Nov 2020 03:36:14 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zathura-ps
[0;1mVersion         :[0m 0.2.6-2
[0;1mDescription     :[0m Adds ps support to zathura by using the libspectre library
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://pwmt.org/projects/zathura-ps
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m libspectre  zathura  desktop-file-utils
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m zathura
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 19.30 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Mon 25 May 2020 06:38:21 PM MSK
[0;1mInstall Date    :[0m Wed 25 Nov 2020 03:36:14 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zsh
[0;1mVersion         :[0m 5.8-1
[0;1mDescription     :[0m A very advanced and programmable command interpreter (shell) for UNIX
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.zsh.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m pcre  libcap  gdbm
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m zsh-autosuggestions  zsh-completions  zsh-history-substring-search  zsh-syntax-highlighting  zsh-theme-powerlevel10k
[0;1mOptional For    :[0m fzf  shell-color-scripts
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 6.22 MiB
[0;1mPackager        :[0m Pierre Schmitz <pierre@archlinux.de>
[0;1mBuild Date      :[0m Sun 16 Feb 2020 08:55:21 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:26:43 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zsh-autosuggestions
[0;1mVersion         :[0m 0.6.4-1
[0;1mDescription     :[0m Fish-like autosuggestions for zsh
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/zsh-users/zsh-autosuggestions
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m zsh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 41.72 KiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jan 2020 01:57:10 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:15:16 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zsh-completions
[0;1mVersion         :[0m 0.32.0-1
[0;1mDescription     :[0m Additional completion definitions for Zsh
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/zsh-users/zsh-completions
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m zsh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 1156.78 KiB
[0;1mPackager        :[0m Antonio Rojas <arojas@archlinux.org>
[0;1mBuild Date      :[0m Sat 06 Jun 2020 04:14:48 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 11:29:07 AM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zsh-doc
[0;1mVersion         :[0m 5.8-1
[0;1mDescription     :[0m Info, HTML and PDF format of the ZSH documentation
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://www.zsh.org/
[0;1mLicenses        :[0m custom
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 6.00 MiB
[0;1mPackager        :[0m Pierre Schmitz <pierre@archlinux.de>
[0;1mBuild Date      :[0m Sun 16 Feb 2020 08:55:21 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:15:15 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zsh-history-substring-search
[0;1mVersion         :[0m 1.0.2-1
[0;1mDescription     :[0m ZSH port of Fish history search (up arrow)
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/zsh-users/zsh-history-substring-search
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m zsh
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 36.07 KiB
[0;1mPackager        :[0m David Runge <dvzrv@archlinux.org>
[0;1mBuild Date      :[0m Sun 27 Oct 2019 04:52:12 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:15:16 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zsh-lovers
[0;1mVersion         :[0m 0.9.1-3
[0;1mDescription     :[0m A collection of tips, tricks and examples for the Z shell.
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://grml.org/zsh/#zshlovers
[0;1mLicenses        :[0m GPL
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m None
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 18.82 KiB
[0;1mPackager        :[0m Felix Yan <felixonmars@archlinux.org>
[0;1mBuild Date      :[0m Tue 07 Jul 2020 07:44:13 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:15:16 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zsh-syntax-highlighting
[0;1mVersion         :[0m 0.7.1-1
[0;1mDescription     :[0m Fish shell like syntax highlighting for Zsh
[0;1mArchitecture    :[0m any
[0;1mURL             :[0m https://github.com/zsh-users/zsh-syntax-highlighting
[0;1mLicenses        :[0m BSD
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m zsh>=4.3.9
[0;1mOptional Deps   :[0m None
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m None
[0;1mInstalled Size  :[0m 139.85 KiB
[0;1mPackager        :[0m Daniel M. Capella <polyzen@archlinux.org>
[0;1mBuild Date      :[0m Tue 17 Mar 2020 04:05:30 AM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:15:16 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m Yes
[0;1mValidated By    :[0m Signature

[0;1mName            :[0m zsh-theme-powerlevel10k
[0;1mVersion         :[0m 1.13.0-1
[0;1mDescription     :[0m Powerlevel10k is a theme for Zsh. It emphasizes speed, flexibility and out-of-the-box experience.
[0;1mArchitecture    :[0m x86_64
[0;1mURL             :[0m https://github.com/romkatv/powerlevel10k
[0;1mLicenses        :[0m MIT
[0;1mGroups          :[0m None
[0;1mProvides        :[0m None
[0;1mDepends On      :[0m glibc  zsh
[0;1mOptional Deps   :[0m powerline-fonts: patched fonts for powerline
                  awesome-terminal-fonts: icon package
[0;1mRequired By     :[0m None
[0;1mOptional For    :[0m None
[0;1mConflicts With  :[0m None
[0;1mReplaces        :[0m zsh-theme-powerlevel9k
[0;1mInstalled Size  :[0m 3.77 MiB
[0;1mPackager        :[0m Christian Rebischke <Chris.Rebischke@archlinux.org>
[0;1mBuild Date      :[0m Sun 18 Oct 2020 02:09:13 PM MSK
[0;1mInstall Date    :[0m Sat 07 Nov 2020 12:15:16 PM MSK
[0;1mInstall Reason  :[0m Explicitly installed
[0;1mInstall Script  :[0m No
[0;1mValidated By    :[0m Signature

